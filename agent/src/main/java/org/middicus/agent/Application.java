package org.middicus.agent;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.middicus.agent.api.Handles;
import org.middicus.agent.api.MessageHandler;
import org.middicus.agent.messaging.ConnectionManager;
import org.middicus.agent.messaging.ConnectionState;
import org.middicus.agent.messaging.MessageAcceptor;
import org.middicus.agent.xfer.TransferThread;


import eu.infomas.annotation.AnnotationDetector;
import eu.infomas.annotation.AnnotationDetector.TypeReporter;

public class Application {
	
	protected final Thread mainThread = Thread.currentThread();
	
	protected static final Application app = new Application();
	
	private Logger log = Logger.getLogger(Application.class);
	private ConnectionManager cm = null;
	
	private String agentName = null;
	private String agentHash = null;
	
	private String queueAnnounce = "";
	private String queueResponse = "";
	private String queueXfer = "";
	
	private AtomicBoolean doRun = new AtomicBoolean(false);
	private boolean resetFlag = false;
	
	private long reconnectInterval;
	private HashSet<DisconnectListener> disconnectListeners = new HashSet<>();
	private HashMap<String, MessageHandler> messageHandlers;
	
	private MessageAcceptor acceptor = null;
	
	private TransferThread transferThread = null;
	
	public Application() {
	}
	
	public static Application getApp() {
		return app;
	}
	
	public ConnectionManager getConnectionManager() {
		return cm;
	}
	
	public String getAgentName() {
		return agentName;
	}

	public String getAgentHash() {
		return agentHash;
	}
	
	public String getQueueAnnounce() {
		return queueAnnounce;
	}

	public String getQueueResponse() {
		return queueResponse;
	}
	
	public String getQueueXfer() {
		return queueXfer;
	}

	public HashSet<DisconnectListener> getDisconnectListeners() {
		return disconnectListeners;
	}
	
	public TransferThread getTransferThread() {
		return transferThread;
	}
	
	/**
	 * <p>Register a listener to receive notice that the controller connection will be shut down.</p>
	 * <p>LISTENERS WILL ONLY BE NOTIFIED ONCE. They must re-register if they need to receive further
	 * @param listener
	 */
	public void addDisconnectListener(DisconnectListener listener) {
		disconnectListeners.add(listener);
	}
	
	public void removeDisconnectListener(DisconnectListener listener) {
		disconnectListeners.remove(listener);
	}

	/**
	 * <p>Agent main program begins here. The bootstrap class will have set up logging and read the config
	 * file for us at this point. We can throw a ConfigException back to main() however, if initialization fails.</p>
	 * 
	 * <p>If configuration is correct and complete, we should never throw an exception back to main
	 * or terminate execution. Once we get to run() the program should NEVER exit except when coerced to
	 * via a shutdown instruction or TERM signal.</p>
	 */
	public void start() throws ConfigException {
		// Get the reconnect interval
		String intervalString = Config.getControllerParam("reconnectIntervalSeconds");
		try {
			reconnectInterval = Long.parseLong(intervalString) * 1000l;
		} catch (NumberFormatException nfe) {
			throw new ConfigException("Value for reconnectIntervalSeconds is not a number: " + intervalString, nfe);
		}
		log.debug("Reconnection interval set to " + intervalString + " seconds");
		
		// Get queue configurations
		queueAnnounce = Config.getControllerParam("queueAnnounce");
		queueResponse = Config.getControllerParam("queueResponse");
		queueXfer = Config.getControllerParam("queueXfer");
		
		// Scan for message handlers
		log.debug("Scanning for message handlers");
		messageHandlers = discoverMessageHandlers();
		
		// Get agent name and hash
		setAgentName();
		
		try {
			loadAgentHash();
		} catch (ConfigException ce) {
			log.fatal(ce.getMessage(), ce);
		}
		
		// Initialize the connection manager
		cm = new ConnectionManager();
		
		// Initialize and start the TransferThread
		transferThread = new TransferThread();
		transferThread.setFileTransferTimeoutMillis(Integer.parseInt(Config.getProcessorParam("fileTransferTimeoutMillis")));
		transferThread.setFileTransferTimeoutScanIntervalMillis(Integer.parseInt(Config.getProcessorParam("fileTransferTimeoutScanIntervalMillis")));
		transferThread.start();
		
		// Start the main execution loop
		doRun.set(true);
		resetFlag = true;
		Runtime.getRuntime().addShutdownHook(new AgentShutdownThread());
		run();
	}
	
	private void run() {
		while(doRun.get()) {
			while (resetFlag == false) {
				try { synchronized (this) { wait(); } }
				catch (InterruptedException ie) {
					log.debug("Main loop wait() interrupted");
				}
			}
			
			log.trace("Reset flag was set, resetting connection to controller");
			resetConnection();
		}
		
		log.debug("Fell out of main run loop, program will terminate");
	}
	
	/**
	 * <p>Reset the controller connection. This call wakes the main thread and allows it to handle
	 * the reconnection.</p>
	 */
	public synchronized void reset() {
		resetFlag = true;
		notify();
	}
	
	public void stop() {
		log.info("Agent instructed to terminate");
		doRun.set(false);
		reset();
	}
	
	/**
	 * <p>Establish, or re-establish connection to the controller.</p>
	 */
	private void resetConnection() {
		// Toss the message acceptor, it should catch the disconnect notice to follow and clean itself up
		acceptor = null;
		
		// Force a disconnect
		log.debug("Disconnecting from controller");
		cm.disconnect();
		
		// If doRun is false, we re staying disconnected
		if (doRun.get() == false) {
			log.trace("doRun is false, staying disconnected from controller");
			return;
		}
		
		// Clear the reset flag
		synchronized (this) { resetFlag = false; }
		
		while (cm.getConnectionState() != ConnectionState.CONNECTED && doRun.get() == true) {
			try { cm.connect(); }
			catch (JMSException je) {
				log.error("Failed to create controller connection", je);
				try { Thread.sleep(reconnectInterval); }
				catch (InterruptedException ie) { log.info("Interrupted while waiting to re-attempt controller connection"); }
			}
		}
		
		// Create a message acceptor
		acceptor = new MessageAcceptor(messageHandlers);
		
		try {
			acceptor.init(cm.getConnection(), cm.getAgentQueueName());
		} catch (JMSException je) {
			log.error("MessageAcceptor init failed after successful connection to controller", je);
			resetFlag = true;
			try { Thread.sleep(reconnectInterval); }
			catch (InterruptedException ie) { log.info("Interrupted while waiting to re-attempt controller connection"); }
			return;
		}
		
		addDisconnectListener(acceptor);
		acceptor.start();
	}
	
	/**
	 * <p>Set the agentName using the config file. If the specified name is "_auto", attempt
	 * to automatically set the name by determining the FQDN of the host.</p>
	 * @throws ConfigException
	 */
	private void setAgentName() throws ConfigException {
		String configName = Config.getAgentParam("agentName");
		if (configName.equals("_auto")) {
			try {
				agentName = InetAddress.getLocalHost().getCanonicalHostName();
				boolean nameLooksRight = false;
				
				int d1 = agentName.indexOf('.');
				if (d1 != -1) {
					int d2 = agentName.indexOf('.', d1);
					if (d2 != -1) {
						nameLooksRight = true;
					}
				}
				
				if (nameLooksRight == false) {
					throw new ConfigException("Automatically determined agent name does not look legit (has not two dots in name): " + agentName);
				}
			} catch (Exception e) {
				throw new ConfigException("agentName set to _auto, but unable to determine this host's name", e);
			}
		} else {
			agentName = configName;
		}
	}
	
	/**
	 * <p>Load the existing agent hash from the hash file in the agent data directory. If it does not exist,
	 * create a new hash and write it to the hash file.</p>
	 * @throws ConfigException If the config is incomplete or reading/writing the hash file fails
	 */
	private void loadAgentHash() throws ConfigException {
		File hashFile = new File(Config.getAgentParam("dataDir"), "agenthash");
		if (hashFile.getParentFile().canWrite() == false) {
			throw new ConfigException("Cannot write hash file, make sure parent directory exists and that permissions are correct: " + hashFile.getAbsolutePath());
		}
		log.debug("Agent hash file: " + hashFile.getAbsolutePath());
		
		// If the hashFile exists, try to read the existing hash from it, otherwise we will create one
		if (hashFile.exists() == true) {
			log.debug("Hash file exists, reading hash");
			FileReader hashReader = null;
			
			try {
				hashReader = new FileReader(hashFile);
				char[] buf = new char[36];
				int cread = hashReader.read(buf);
				if (cread < 36) {
					throw new ConfigException("Hash file contained less than 36 characters, hashes are exactly 36 characters. " + hashFile.getAbsolutePath());
				}
				agentHash = new String(buf);
				log.debug("Read agent hash: " + agentHash);
			} catch (IOException ioe) {
				agentHash = null;
				throw new ConfigException("I/O error reading agent hash file: " + hashFile.getAbsolutePath(), ioe);
			} finally {
				log.debug("Closing hash file reader");
				if (hashReader != null) { try { hashReader.close(); } catch (Exception e) {} }
			}
		}
		// The hashFile does not exist, create a new hash
		else {
			agentHash = UUID.randomUUID().toString();
			log.info("Newly genereated hash for this agent: " + agentHash);
			FileWriter hashWriter = null;
			
			try {
				hashWriter = new FileWriter(hashFile);
				hashWriter.write(agentHash);
			} catch (IOException ioe) {
				agentHash = null;
				throw new ConfigException("Could not write new hash to hash file: " + hashFile.getAbsolutePath());
			} finally {
				log.debug("Closing hash file writer");
				if (hashWriter != null) { try { hashWriter.close(); } catch (Exception e) { } }
			}
		}
	}
	
	private HashMap<String, MessageHandler> discoverMessageHandlers() {
		long scanStart = System.currentTimeMillis();
		
		final HashSet<Class<?>> handlerClasses = new HashSet<>();
		
		TypeReporter reporter = new TypeReporter() {
			
			@SuppressWarnings("unchecked")
			@Override
			public Class<? extends Annotation>[] annotations() {
				return new Class[] { Handles.class };
			}
			
			@Override
			public void reportTypeAnnotation(Class<? extends Annotation> annotation, String className) {
				try {
					log.debug("Annotation scanner identified Handles annotated class: " + className);
					Class<?> handlerClass = Class.forName(className);
					handlerClasses.add(handlerClass);
				} catch (ClassNotFoundException cnfe) {
					log.error("Annotation scanner found className that could not be resolve to a Class: " + className, cnfe);
				}
			}
		};
		
		AnnotationDetector detector = new AnnotationDetector(reporter);
		try {
			detector.detect();
		} catch (IOException ioe) {
			log.error("I/O error scanning for message handlers", ioe);
		}
		
		HashMap<String, MessageHandler> handlers = new HashMap<>(handlerClasses.size());
		for (Class<?> handlerClass : handlerClasses) {
			Handles ano = handlerClass.getAnnotation(Handles.class);
			String msgType = ano.messageType();
			if (msgType == null || msgType.length() == 0) {
				log.error("MessageHandler " + handlerClass.getName() + " missing valid Handles annotation");
			} else {
				try {
					MessageHandler handler = (MessageHandler)handlerClass.newInstance();
					log.info("Added MessageHandler " + handlerClass.getSimpleName() + " for message type " + msgType);
					handlers.put(msgType, handler);
				} catch (Exception e) {
					log.error("Could not create instance of MessageHandler " + handlerClass.getName());
				}
			}
		}
		
		long scanTime = System.currentTimeMillis() - scanStart;
		log.info("MessageHandler scan completed in " + scanTime + "ms");
		
		return handlers;
	}
	
	class AgentShutdownThread extends Thread {
		
		private Logger log = Logger.getLogger(AgentShutdownThread.class);
		
		@Override
		public void run() {
			log.info("Shutdown handler invoked, stopping agent");
			Application.this.stop();
			Application.this.mainThread.interrupt();
			try { Application.this.mainThread.join(); }
			catch (InterruptedException ie) { log.trace("mainThread.join() interrupted"); }
			log.info("Shutdown complete");
		}
	}
}
