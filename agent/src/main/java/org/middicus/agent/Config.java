package org.middicus.agent;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class Config {
	
	public static HashMap<String, String> agentConfig = new HashMap<>(2);
	public static HashMap<String, String> controllerConfig = new HashMap<>(6);
	public static HashMap<String, String> processorConfig = new HashMap<>(2);
	
	public static void loadConfig(File agentXmlFile) throws IOException {
		FileReader configReader = null;
		
		try {
			configReader = new FileReader(agentXmlFile);
			XMLReader xr = XMLReaderFactory.createXMLReader();
			xr.setContentHandler(new ConfigHandler());
			xr.parse(new InputSource(configReader));
		} catch (SAXException se) {
			throw new IOException("Parsing error", se);
		} finally {
			try { configReader.close(); } catch (IOException ioe) { }
		}
	}
	
	public static String getAgentParam(String name) throws ConfigException {
		if (agentConfig.containsKey(name) == false) {
			throw new ConfigException("Required <agent> configuration parameter not present: " + name);
		}
		
		return agentConfig.get(name);
	}
	
	public static String getControllerParam(String name) throws ConfigException {
		if (controllerConfig.containsKey(name) == false) {
			throw new ConfigException("Required <controller> configuration parameter not present: " + name);
		}
		
		return controllerConfig.get(name);
	}
	
	public static String getProcessorParam(String name) throws ConfigException {
		if (processorConfig.containsKey(name) == false) {
			throw new ConfigException("Required <processor> configuration parameter not present: " + name);
		}
		
		return processorConfig.get(name);
	}
}
