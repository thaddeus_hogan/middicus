package org.middicus.agent;

public class ConfigException extends Exception {
	public static final long serialVersionUID = 1l;
	
	public ConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfigException(String message) {
		super(message);
	}
	
}
