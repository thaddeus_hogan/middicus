package org.middicus.agent;

import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigHandler extends DefaultHandler {

	private HashMap<String, String> activeConfig = null;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equals("agent")) {
			if (activeConfig != null) { throw new SAXException("Encountered <agent> unexpectedly, it must reside under root level <config>"); }
			activeConfig = Config.agentConfig;
		} else if (qName.equals("controller")) {
			if (activeConfig != null) { throw new SAXException("Encountered <controller> unexpectedly, it must reside under root level <config>"); }
			activeConfig = Config.controllerConfig;
		} else if (qName.equals("processor")) {
			if (activeConfig != null) { throw new SAXException("Encountered <processor> unexpectedly, it must reside under root level <config>"); }
			activeConfig = Config.processorConfig;
		} else if (qName.equals("param")) {
			if (activeConfig == null) {
				throw new SAXException("Encountered <param> unexpectedly, is must reside under a subsystem configuration.");
			}
			String paramName = attributes.getValue("name");
			String paramValue = attributes.getValue("value");
			if (paramName == null || paramValue == null) {
				throw new SAXException("<param> element requires attributes \"name\" and \"value\"");
			}
			
			activeConfig.put(paramName, paramValue);
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equals("agent") || qName.equals("controller") || qName.equals("processor")) {
			activeConfig = null;
		}
	}
}
