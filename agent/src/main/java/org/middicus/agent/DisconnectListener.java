package org.middicus.agent;

/**
 * <p>Provides for notification of when the agent is disconnecting from the server.</p>
 * <p>Implementations <em>MUST NOT</em> assume that the agent is in a connected state when notified of a disconnect.</p>
 */
public interface DisconnectListener {
	public void preDisconnect();
	public String getListenerName();
}
