package org.middicus.agent;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class Main {

	public static final String PROP_LOG_CONFIG = "log4j.configuration";
	public static final String PROP_AGENT_CONFIG = "dsc.agent.config";
	
	/**
	 * <p>Main has two tasks: Initialize log4j and read the agent configuration. After that,
	 * execution will pass to the Application class, which will create any additional threads
	 * needed and handle global state for the life of the agent.</p>
	 * @param args THE ARGS!
	 */
	public static void main(String[] args) {
		// Initialize logging
		System.err.println("Initializing logging...");
		
		// If log4j.configuration property is set, log4j should just work, otherwise
		// we will configure log4j with the default log4j.xml in the agent JAR
		if (System.getProperty(PROP_LOG_CONFIG) == null) {
			System.err.println("WARNING: Using built in logging configuration. Specify your own with -Dlog4j.configuration");
			URL log4jXml = Thread.currentThread().getContextClassLoader().getResource("log4j.xml");
			DOMConfigurator.configure(log4jXml);
		}
		
		Logger log = Logger.getLogger(Main.class);
		log.info("Logging initialized.");
		
		// Make sure a configuration file is specified and is readable
		String agentXmlFilePath = System.getProperty(PROP_AGENT_CONFIG);
		if (agentXmlFilePath == null) {
			log.fatal("System property " + PROP_AGENT_CONFIG + " must be set to the path of an agent.xml config file");
			System.exit(1);
		}
		
		File agentXmlFile = new File(agentXmlFilePath);
		if (agentXmlFile.canRead() == false) {
			log.fatal("Agent configuration specified is unreadable due to permissions or non-existant");
			log.fatal("Agent configuration file path: " + agentXmlFile.getAbsolutePath());
			System.exit(1);
		}
		
		// Read the configuration file
		log.info("Loading configuration from " + agentXmlFile.getAbsolutePath());
		
		try {
			Config.loadConfig(agentXmlFile);
		} catch (IOException ioe) {
			log.fatal("Loading of configuration file failed.", ioe);
			System.exit(1);
		}
		
		log.info("Configuration loaded, initializing agent application");
		try {
			Application.getApp().start();
		} catch (ConfigException ce) {
			log.fatal(ce.getMessage(), ce);
			System.exit(1);
		}
	}
}
