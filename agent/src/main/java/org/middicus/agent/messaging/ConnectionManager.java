package org.middicus.agent.messaging;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TemporaryQueue;

import org.apache.log4j.Logger;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;
import org.middicus.agent.Application;
import org.middicus.agent.Config;
import org.middicus.agent.ConfigException;
import org.middicus.agent.DisconnectListener;
import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.registration.DSCRegisterMessage;
import org.middicus.messages.registration.DSCRegisterResponse;
import org.middicus.messages.registration.DSCRegisterResponse.RegistrationResponse;


public class ConnectionManager {

	private long registerTTL = 5000;
	
	private Logger log = Logger.getLogger(ConnectionManager.class);
	
	private ConnectionState state = ConnectionState.NOT_CONNECTED;
	private ConnectionFactory factory = null;
	private Connection conn = null;
	
	private String username = "";
	private String password = "";
	private String host = "";
	private int port = 0;
	
	private String agentQueueName = null;
	
	public ConnectionManager() throws ConfigException {
		try { registerTTL = Long.parseLong(Config.getControllerParam("registerTTL")); }
		catch (NumberFormatException nfe) { throw new ConfigException("<controller> param registerTTL is not a number"); }
		
		host = Config.getControllerParam("host");
		try { port = Integer.parseInt(Config.getControllerParam("port")); }
		catch (NumberFormatException nfe) { throw new ConfigException("Value of parameter port in <agent> in config is not parsable as a number", nfe); }
		username = Config.getControllerParam("username");
		password = Config.getControllerParam("password");
		
		log.info("Configured to connect to controller at " + host + ":" + Integer.toString(port));
	}
	
	public synchronized ConnectionState getConnectionState() {
		return state;
	}
	
	protected synchronized void setConnectionState(ConnectionState state) {
		this.state = state;
	}
	
	/**
	 * <p>Get the current connection. This may be null or a connection in a bad state. Any class calling
	 * this method should invoke Application.
	 * @return
	 */
	public Connection getConnection() {
		return conn;
	}
	
	/**
	 * <p>Get the agent's queue name, for use by the MessageAcceptor. This will be provided by the controller
	 * upon successful registration.</p>
	 */
	public String getAgentQueueName() {
		return agentQueueName;
	}
	
	/**
	 * <p>Connect to the configured DSC controller.</p>
	 * @throws ConfigException If the active configuration is incorrect or incomplete
	 * @throws JMSException If the connection fails and state does not transition
	 */
	public void connect() throws JMSException {
		if (getConnectionState() != ConnectionState.NOT_CONNECTED) {
			log.warn("connect() called while not in disconnected state");
			disconnect();
		}
		
		// Configure the HornetQ transport
		Map<String, Object> tpParams = new HashMap<>();
		tpParams.put(TransportConstants.HOST_PROP_NAME, host);
		tpParams.put(TransportConstants.PORT_PROP_NAME, port);
		TransportConfiguration tconf = new TransportConfiguration(NettyConnectorFactory.class.getName(), tpParams);
		
		// Create the factory
		log.trace("Creating connection factory");
		factory = (ConnectionFactory)HornetQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.CF, tconf);
		
		log.debug("Creating connection to controller");
		conn = factory.createConnection(username, password);
		log.info("Connected to controller, registration pending");
		
		log.trace("Starting message consumption");
		conn.start();
		
		// Update state and begin the registration process
		setConnectionState(ConnectionState.REGISTERING);
		register();
		setConnectionState(ConnectionState.CONNECTED);
	}
	
	public Connection createConnection() throws JMSException {
		return factory.createConnection(username, password);
	}
	
	private void register() throws JMSException {
		log.trace("Registration started");
		Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		log.trace("JMS session created");
		
		// Setup the queues, create a temp queue for the registration response
		TemporaryQueue regResponseQ = sess.createTemporaryQueue();
		log.trace("Created registration response queue: " + regResponseQ.getQueueName());
		MessageConsumer consumer = sess.createConsumer(regResponseQ);
		
		// Send the registration message
		DSCRegisterMessage registerMsg = new DSCRegisterMessage(
			Application.getApp().getAgentName(),
			Application.getApp().getAgentHash());
		log.trace("Created registration message: " + registerMsg.toString());
		
		MessageSender.sendMessage(registerMsg, Application.getApp().getQueueAnnounce(), regResponseQ);
		
		// Wait for the registration response
		log.debug("Consumer waiting for registration response for " + registerTTL + "ms");
		ObjectMessage msg = (ObjectMessage)consumer.receive(registerTTL);
		
		if (msg == null) {
			throw new JMSException("Receive of registration response timed out after " + registerTTL + "ms");
		}
		
		DSCMessage regRespSuper = (DSCMessage)msg.getObject();
		if (regRespSuper.getType().equals(CoreMessageType.REGISTER_RESPONSE.toString()) == false) {
			throw new JMSException("Response message to registration was not a REGISTER_RESPONSE type message.");
		}
		
		DSCRegisterResponse regResp = (DSCRegisterResponse)regRespSuper;
		log.info("Registration response: " + regResp.getRegistrationResponse().toString());
		
		if (regResp.getRegistrationResponse() == RegistrationResponse.INVALID_AGENT_HASH) {
			throw new JMSException("Controller reported invalid hash, is this a new box with a re-used host name?");
		} else if (regResp.getRegistrationResponse() == RegistrationResponse.FAILURE) {
			throw new JMSException("Controller failed to register this agent, Check controller logs");
		} else if (regResp.getRegistrationResponse() == RegistrationResponse.SUCCESS) {
			agentQueueName = regResp.getAgentQueueName();
			log.info("Registration successful, agent name is " + Application.getApp().getAgentName());
		}
	}
	
	/**
	 * <p>Enter a disconnected state. This method does not care what state the connection is currently in.</p>
	 */
	public void disconnect() {
		log.debug("Disconnecting from controller");
		
		// Copy the disconnect listeners list and clear the original BEFORE running any of the listners
		// This way is something wants to listen for future disconnect events, they can re-register
		// within the preDisconnect() method.
		HashSet<DisconnectListener> listenersCopy = new HashSet<>(Application.getApp().getDisconnectListeners());
		Application.getApp().getDisconnectListeners().clear();
		
		for (DisconnectListener listener : listenersCopy) {
			log.debug("Calling DisconnectListener " + listener.getListenerName());
			listener.preDisconnect();
		}
		
		if (conn != null) {
			try {
				conn.stop();
			} catch (JMSException je) {
				log.info("Exception on connection stop", je);
			}
			
			try {
				conn.close();
			} catch (JMSException je) {
				log.info("Exception on connection close", je);
			}
		}
		
		conn = null;
		setConnectionState(ConnectionState.NOT_CONNECTED);
	}
}
