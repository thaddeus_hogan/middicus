package org.middicus.agent.messaging;

public enum ConnectionState {
	NOT_CONNECTED,
	REGISTERING,
	CONNECTED
}
