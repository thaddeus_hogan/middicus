package org.middicus.agent.messaging;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.hornetq.api.jms.HornetQJMSClient;
import org.middicus.agent.Application;
import org.middicus.agent.DisconnectListener;
import org.middicus.agent.api.HandlingException;
import org.middicus.agent.api.MessageHandler;
import org.middicus.messages.DSCMessage;


public class MessageAcceptor extends Thread implements DisconnectListener {
	
	private Logger log = Logger.getLogger(MessageAcceptor.class);
	
	private AtomicBoolean doRun = new AtomicBoolean(false);
	private Session sess = null;
	private MessageConsumer consumer = null;
	private HashMap<String, MessageHandler> messageHandlers;

	public MessageAcceptor(HashMap<String, MessageHandler> messageHandlers) {
		setName("MessageAcceptor");
		this.messageHandlers = messageHandlers;
	}
	
	public void init(Connection conn, String agentQueueName) throws JMSException, IllegalThreadStateException {
		if (getState() != State.NEW) {
			throw new IllegalThreadStateException("MessageAcceptor instances may only be used once, do not call init() more than once.");
		}
		
		sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Queue agentQ = HornetQJMSClient.createQueue(agentQueueName);
		consumer = sess.createConsumer(agentQ);
		doRun.set(true);
	}
	
	@Override
	public void run() {
		while (doRun.get()) {
			try {
				Message msg = consumer.receive();
				// FIXME - NPE after here kills agent
				Object msgObject = ((ObjectMessage)msg).getObject();
				DSCMessage dscMsg = (DSCMessage)msgObject;
				
				if (messageHandlers.containsKey(dscMsg.getType()) == false) {
					log.error("No message handler for DSCMessage of type " + dscMsg.getType());
				} else {
					try {
						messageHandlers.get(dscMsg.getType()).handle(dscMsg);
					} catch (HandlingException he) {
						log.error("Handler failed to handle message of type " + dscMsg.getType(), he);
					}
				}
				
			} catch (Exception e) {
				log.warn("consumer.receive() threw an Exception, will reset the controller connection", e);
				doRun.set(false);
				Application.getApp().reset();
			}
		}
		
		log.debug("MessageAcceptor terminating");
		if (sess != null) {
			try { sess.close(); }
			catch (JMSException je) {
				log.debug("Could not close session on possibly dead connection", je);
			}
		}
	}
	
	@Override
	public void preDisconnect() {
		log.debug("Caught disconnect event, attempting to clean up");
		if (getState() != State.TERMINATED) {
			log.debug("Thread for this acceptor is currently running, shutting it down");
			doRun.set(false);
			try { consumer.close(); } catch (JMSException je) { log.debug("consumer.close() threw exception", je); }
			
			log.trace("Acceptor thread interrupted, waiting for it to terminate");
			try { join(); }
			catch (InterruptedException ie) {
				log.error("MessageAcceptor.preDisconnect() interrupted while waiting for thread to die");
			}
		}
		log.debug("MessageAcceptor disconnect listener completed");
	}
	
	@Override
	public final String getListenerName() {
		return getName();
	}
}
