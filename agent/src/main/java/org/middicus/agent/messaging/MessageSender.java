package org.middicus.agent.messaging;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.hornetq.api.jms.HornetQJMSClient;
import org.middicus.agent.Application;
import org.middicus.messages.DSCMessage;


public class MessageSender {
	
	private static Logger log = Logger.getLogger(MessageSender.class);
	
	public static void sendMessage(DSCMessage dscMsg, String destQueueName) throws JMSException {
		sendMessage(dscMsg, destQueueName, null);
	}
	
	public static void sendMessage(DSCMessage dscMsg, String destQueueName, Queue replyQueue) throws JMSException {
		Connection conn = Application.getApp().getConnectionManager().getConnection();
		if (conn == null) { throw new JMSException("Could not send message, not connected to controller"); }
		
		Queue destQ = HornetQJMSClient.createQueue(destQueueName);
		Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer producer = sess.createProducer(destQ);
		
		if (dscMsg.isDurableHint()) {
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);
		} else {
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		}
		
		if (dscMsg.getTtlHint() > 0) {
			producer.setTimeToLive(dscMsg.getTtlHint());
		}
		
		if (replyQueue != null) {
			ObjectMessage msg = sess.createObjectMessage(dscMsg);
			msg.setJMSReplyTo(replyQueue);
			producer.send(msg);
		} else {
			producer.send(sess.createObjectMessage(dscMsg));
		}
		
		producer.close();
		sess.close();
	}
}
