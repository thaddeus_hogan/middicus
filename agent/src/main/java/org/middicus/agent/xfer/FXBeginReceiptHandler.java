package org.middicus.agent.xfer;

import java.io.IOException;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.middicus.agent.Application;
import org.middicus.agent.api.Handles;
import org.middicus.agent.api.HandlingException;
import org.middicus.agent.api.MessageHandler;
import org.middicus.agent.messaging.MessageSender;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.xfer.FXBeginReceipt;
import org.middicus.messages.xfer.FXRequestChunk;
import org.middicus.xfer.FileTransfer;
import org.middicus.xfer.FileTransfer.Role;


@Handles(messageType = "CORE_FX_BEGIN_RECEIPT")
public class FXBeginReceiptHandler implements MessageHandler {
	
	private Logger log = Logger.getLogger(FXBeginReceiptHandler.class);
	
	@Override
	public void handle(DSCMessage msgSuper) throws HandlingException {
		FXBeginReceipt msg = (FXBeginReceipt)msgSuper;
		FileTransfer transfer = new FileTransfer(msg.getXferId(), Role.WRITING, msg.getTargetPath(), msg.getChunkSizeBytes());
		transfer.setPostWriteHandler(Application.getApp().getTransferThread().getStandardPostWriteHandler());
		
		// Try to init the file transfer, if this fails notify the sender to end the transfer
		try {
			transfer.init();
		} catch (IOException ioe) {
			Application.getApp().getTransferThread().getStandardPostWriteHandler().onError(transfer.getXferId(), ioe);
		}
		
		// Provide the prepared FileTransfer to the TransferThread and request the first chunk
		Application.getApp().getTransferThread().addFileTransfer(transfer);
		
		FXRequestChunk reqMsg = new FXRequestChunk(Application.getApp().getAgentName(), transfer.getXferId(), 0);
		try {
			MessageSender.sendMessage(reqMsg, Application.getApp().getQueueXfer());
		} catch (JMSException je) {
			log.error("Failed to request first chunk for FileTransfer: " + transfer.getXferId(), je);
		}
	}
}
