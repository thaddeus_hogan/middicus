package org.middicus.agent.xfer;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.middicus.agent.Application;
import org.middicus.agent.api.Handles;
import org.middicus.agent.api.HandlingException;
import org.middicus.agent.api.MessageHandler;
import org.middicus.agent.messaging.MessageSender;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.xfer.FXChunkMessage;
import org.middicus.messages.xfer.FXEndReceipt;


@Handles(messageType = "CORE_FX_CHUNK")
public class FXChunkMessageHandler implements MessageHandler {

	private Logger log = Logger.getLogger(FXChunkMessageHandler.class);
	
	@Override
	public void handle(DSCMessage msgSuper) throws HandlingException {
		FXChunkMessage msg = (FXChunkMessage)msgSuper;
		log.trace("Chunk received for FileTransfer " + msg.getXferId());
		
		// CRC check happens here so that we aren't holding up the monitor lock on TransferThread while computing this
		if (msg.compareCrc() == false) {
			// CRC check on incoming data failed
			String errStr = "FileTransfer " + msg.getXferId() + " failed CRC32 check for chunk " + Long.toString(msg.getChunkNum());
			
			log.error(errStr);
			FXEndReceipt endMsg = new FXEndReceipt(Application.getApp().getAgentName(), msg.getXferId(), true, errStr);
			
			try {
				MessageSender.sendMessage(endMsg, Application.getApp().getQueueXfer());
			} catch (JMSException je) {
				log.error("Failed to alert sender of FileTransfer ending in error", je);
			}
		} else {
			// CRC check on incoming data looks good, send the chunk for writing
			Application.getApp().getTransferThread().addChunk(msg);
		}
	}

}
