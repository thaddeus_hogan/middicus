package org.middicus.agent.xfer;

import org.middicus.agent.Application;
import org.middicus.agent.api.Handles;
import org.middicus.agent.api.HandlingException;
import org.middicus.agent.api.MessageHandler;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.xfer.FXEndReceipt;


@Handles(messageType = "CORE_FX_END_RECEIPT")
public class FXEndReceiptHandler implements MessageHandler {

	@Override
	public void handle(DSCMessage msgSuper) throws HandlingException {
		FXEndReceipt msg = (FXEndReceipt)msgSuper;
		Application.getApp().getTransferThread().endFileTransfer(msg.getXferId(), msg.isError(), msg.getMessage());
	}

}
