package org.middicus.agent.xfer;

import java.util.HashMap;
import java.util.LinkedList;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.middicus.agent.Application;
import org.middicus.agent.messaging.MessageSender;
import org.middicus.messages.xfer.FXChunkMessage;
import org.middicus.messages.xfer.FXEndReceipt;
import org.middicus.messages.xfer.FXRequestChunk;
import org.middicus.xfer.FileTransfer;
import org.middicus.xfer.PostWriteHandler;


public class TransferThread extends Thread {
	
	private Logger log = Logger.getLogger(TransferThread.class);

	private boolean doRun = false;
	
	private int fileTransferTimeoutScanIntervalMillis = 10000;
	private int fileTransferTimeoutMillis = 60000;
	
	private long nextTimeoutScan = 0l;
	
	private PostWriteHandler standardPostWriteHandler = new StandardPostWriteHandler();

	private LinkedList<FXChunkMessage> incomingChunks = new LinkedList<>();
	private LinkedList<FXRequestChunk> incomingRequests = new LinkedList<>();
	
	private HashMap<String, FileTransfer> transfers = new HashMap<>();
	
	public TransferThread() {
		setDaemon(true);
		setName("TransferThread");
	}
	
	public void setFileTransferTimeoutMillis(int fileTransferTimeoutMillis) {
		this.fileTransferTimeoutMillis = fileTransferTimeoutMillis;
	}
	
	public void setFileTransferTimeoutScanIntervalMillis(
			int fileTransferTimeoutScanIntervalMillis) {
		this.fileTransferTimeoutScanIntervalMillis = fileTransferTimeoutScanIntervalMillis;
	}
	
	public PostWriteHandler getStandardPostWriteHandler() {
		return standardPostWriteHandler;
	}
	
	@Override
	public synchronized void start() {
		nextTimeoutScan = System.currentTimeMillis() + fileTransferTimeoutScanIntervalMillis;
		doRun = true;
		log.info("TransferThread started");
		super.start();
	}
	
	public synchronized void addChunk(FXChunkMessage newChunk) {
		incomingChunks.offer(newChunk);
		notify();
	}
	
	public synchronized void addRequest(FXRequestChunk newRequest) {
		incomingRequests.offer(newRequest);
		notify();
	}
	
	public void addFileTransfer(FileTransfer fileTransfer) {
		transfers.put(fileTransfer.getXferId(), fileTransfer);
	}
	
	/**
	 * <p>End a file transfer in response to an FXEndReceipt message.</p>
	 * @param xferId FileTransfer id to end
	 * @param error True if the file transfer ended due to an error, false if successful completion
	 * @param message Any message the other side wished to include, may be null
	 */
	public void endFileTransfer(String xferId, boolean error, String message) {
		if (message == null) { message = ""; }
		if (error) { log.error("FileTransfer " + xferId + " ended in error: " + message); }
		else { log.info("FileTransfer " + xferId + " ended successfully: " + message); }
		
		FileTransfer xfer = transfers.get(xferId);
		if (xfer != null) {
			transfers.remove(xferId);
			xfer.close();
		} else {
			log.warn("FileTransfer end notification for FileTransfer that did not exist: " + xferId);
		}
	}
	
	@Override
	public void run() {
		while (doRun == true) {
			// Wait, until interrupted or it's time to scan for timed out transfers
			long waitTime = nextTimeoutScan - System.currentTimeMillis();
			if (waitTime > 0) {
				synchronized (this) {
					if (incomingChunks.size() == 0 && incomingRequests.size() == 0) { // Only wait if there were no new incoming chunks or requests
						try { this.wait(waitTime); } catch (InterruptedException ie) { }
					}
				}
			}
			
			// Process incoming chunks
			FXChunkMessage chunk = null;
			do {
				synchronized (this) { chunk = incomingChunks.poll(); }
				if (chunk != null) {
					if (transfers.containsKey(chunk.getXferId())) {
						FileTransfer xfer = transfers.get(chunk.getXferId());
						xfer.writeChunk(chunk.getChunkNum(), chunk.getChunkData());
					} else {
						log.error("Chunk arrived for non-existent transfer id: " + chunk.getXferId());
					}
				}
			} while (chunk != null);
			
			// Process incoming requests
			FXRequestChunk request = null;
			do {
				synchronized (this) { request = incomingRequests.poll(); }
				if (request != null) {
					if (transfers.containsKey(request.getXferId())) {
						FileTransfer xfer = transfers.get(request.getXferId());
						xfer.readChunk(request.getChunkNum());
					} else {
						log.error("Chunk request for non-existent transfer id: " + request.getXferId());
					}
				}
			} while (request != null);
			
			// Scan for timeouts if it is time to do so
			if (System.currentTimeMillis() >= nextTimeoutScan) {
				log.trace("Scanning for timed out file transfers");
				for (String xferId : transfers.keySet()) {
					FileTransfer xfer = transfers.get(xferId);
					if (xfer != null) {
						if ((System.currentTimeMillis() - xfer.getLastActivityTime()) >= fileTransferTimeoutMillis) {
							log.warn("Timed out file transfer: " + xferId);
							transfers.remove(xferId);
							xfer.timeout();
						}
					}
				}
				
				nextTimeoutScan = System.currentTimeMillis() + fileTransferTimeoutScanIntervalMillis;
			}
		}
	}
	
	/**
	 * <p>Basic PostWriteHandler implementation suitable for most file transfers.</p>
	 */
	public class StandardPostWriteHandler implements PostWriteHandler {
		
		private Logger log = Logger.getLogger(StandardPostWriteHandler.class);
		
		@Override
		public void onSuccess(String xferId, long chunkNum) {
			log.trace("Wrote chunk " + Long.toString(chunkNum) + ", requesting next chunk");
			FXRequestChunk reqMsg = new FXRequestChunk(Application.getApp().getAgentName(), xferId, chunkNum + 1);
			
			try {
				MessageSender.sendMessage(reqMsg, Application.getApp().getQueueXfer());
			} catch (JMSException je) {
				log.error("Failed to send next chunk request", je);
			}
		}
		
		@Override
		public void onError(String xferId, Exception e) {
			log.error("Error in file transfer " + xferId, e);
			FXEndReceipt endMsg = new FXEndReceipt(Application.getApp().getAgentName(), xferId, true, e.getMessage());
			
			try {
				MessageSender.sendMessage(endMsg, Application.getApp().getQueueXfer());
			} catch (JMSException je) {
				log.error("Failed to signal sender of file transfer error", je);
			}
		}
	}
}
