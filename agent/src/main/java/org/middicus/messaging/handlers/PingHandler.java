package org.middicus.messaging.handlers;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.middicus.agent.Application;
import org.middicus.agent.api.Handles;
import org.middicus.agent.api.HandlingException;
import org.middicus.agent.api.MessageHandler;
import org.middicus.agent.messaging.MessageSender;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.registration.DSCPingMessage;


@Handles(messageType = "CORE_PING")
public class PingHandler implements MessageHandler {

	private Logger log = Logger.getLogger(PingHandler.class);
	
	@Override
	public void handle(DSCMessage msgSuper) throws HandlingException {
		log.trace("Ping message from controller, responding with ping to announce queue");
		DSCPingMessage ping = new DSCPingMessage(Application.getApp().getAgentName());
		try { MessageSender.sendMessage(ping, Application.getApp().getQueueAnnounce()); }
		catch (JMSException je) { throw new HandlingException("Exception sending ping to announce queue", je); }
	}

}
