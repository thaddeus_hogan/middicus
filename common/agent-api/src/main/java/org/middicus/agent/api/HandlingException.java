package org.middicus.agent.api;

public class HandlingException extends Exception {
	public static final long serialVersionUID = 1l;

	public HandlingException(String message, Throwable cause) {
		super(message, cause);
	}

	public HandlingException(String message) {
		super(message);
	}
}
