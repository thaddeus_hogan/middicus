package org.middicus.agent.api;

import org.middicus.messages.DSCMessage;

public interface MessageHandler {
	public void handle(DSCMessage msgSuper) throws HandlingException;
}
