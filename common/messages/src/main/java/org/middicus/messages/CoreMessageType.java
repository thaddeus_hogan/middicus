package org.middicus.messages;

public enum CoreMessageType {
	PING("CORE_PING"),
	REGISTER("CORE_REGISTER"),
	REGISTER_RESPONSE("CORE_REGISTER_RESPONSE"),
	TASK("CORE_TASK"),
	TASK_RESPONSE("CORE_TASK_RESPONSE"),
	DISPATCH("CORE_DISPATCH"),
	
	FX_CHUNK("CORE_FX_CHUNK"),
	FX_BEGIN_RECEIPT("CORE_FX_BEGIN_RECEIPT"),
	FX_REQUEST_CHUNK("CORE_FX_REQUEST_CHUNK"),
	FX_END_RECEIPT("CORE_FX_END_RECEIPT"),
	FX_INITIATE_TRANSFER("CORE_FX_INITIATE_TRANSFER");
	
	private final String msgTypeName;
	private CoreMessageType(String msgTypeName) {
		this.msgTypeName = msgTypeName;
	}
	
	@Override
	public String toString() {
		return msgTypeName;
	}
}
