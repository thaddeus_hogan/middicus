package org.middicus.messages;

import java.io.Serializable;

public abstract class DSCMessage implements Serializable {
	public static final long serialVersionUID = 1l;
	
	protected String sender;
	protected String type;
	
	protected transient String destAgent;
	protected transient long ttlHint;
	protected transient boolean durableHint;
	
	public DSCMessage(String sender, String type) {
		this.sender = sender;
		this.type = type;
		destAgent = null;
		ttlHint = 0;
		durableHint = true;
	}

	public String getSender() {
		return sender;
	}

	public String getType() {
		return type;
	}

	public String getDestAgent() {
		return destAgent;
	}

	public DSCMessage setDestAgent(String destAgent) {
		this.destAgent = destAgent;
		return this;
	}

	public long getTtlHint() {
		return ttlHint;
	}

	public DSCMessage setTtlHint(long ttlHint) {
		this.ttlHint = ttlHint;
		return this;
	}

	public boolean isDurableHint() {
		return durableHint;
	}

	public DSCMessage setDurableHint(boolean durableHint) {
		this.durableHint = durableHint;
		return this;
	}
}
