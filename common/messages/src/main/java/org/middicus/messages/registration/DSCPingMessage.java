package org.middicus.messages.registration;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class DSCPingMessage extends DSCMessage {

	private static final long serialVersionUID = 1l;

	public DSCPingMessage(String sender) {
		super(sender, CoreMessageType.PING.toString());
	}

}
