package org.middicus.messages.registration;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class DSCRegisterMessage extends DSCMessage {
	public static final long serialVersionUID = 1l;

	protected String agentHash;
	
	public DSCRegisterMessage(String sender, String agentHash) {
		super(sender, CoreMessageType.REGISTER.toString());
		this.agentHash = agentHash;
	}

	public String getAgentHash() {
		return agentHash;
	}
	
	@Override
	public String toString() {
		return "Agent Name: " + sender + " | Agent Hash: " + agentHash;
	}
}
