package org.middicus.messages.registration;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class DSCRegisterResponse extends DSCMessage {
	public static final long serialVersionUID = 1l;

	private RegistrationResponse registrationResponse;
	private String agentQueueName;
	
	public DSCRegisterResponse(String sender, RegistrationResponse registrationResponse, String agentQueueName) {
		super(sender, CoreMessageType.REGISTER_RESPONSE.toString());
		this.registrationResponse = registrationResponse;
		this.agentQueueName = agentQueueName;
	}
	
	public RegistrationResponse getRegistrationResponse() {
		return registrationResponse;
	}
	
	public String getAgentQueueName() {
		return agentQueueName;
	}
	
	public enum RegistrationResponse {
		SUCCESS,
		INVALID_AGENT_HASH,
		FAILURE
	}
}
