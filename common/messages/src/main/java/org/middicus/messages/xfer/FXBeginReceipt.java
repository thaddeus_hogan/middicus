package org.middicus.messages.xfer;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class FXBeginReceipt extends DSCMessage {
	
	public static final long serialVersionUID = 1l;
	
	private String xferId;
	private String targetPath;
	private int chunkSizeBytes;
	
	public FXBeginReceipt(String sender, String xferId, String targetPath, int chunkSizeBytes) {
		super(sender, CoreMessageType.FX_BEGIN_RECEIPT.toString());
		
		this.xferId = xferId;
		this.targetPath = targetPath;
		this.chunkSizeBytes = chunkSizeBytes;
	}

	public String getXferId() {
		return xferId;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public int getChunkSizeBytes() {
		return chunkSizeBytes;
	}
}
