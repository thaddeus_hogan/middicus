package org.middicus.messages.xfer;

import java.util.zip.CRC32;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;


public class FXChunkMessage extends DSCMessage {
	
	public static final long serialVersionUID = 1l;
	
	private String xferId;
	private long chunkNum;
	private byte[] chunkData;
	private long crc;
	
	public FXChunkMessage(String sender, String xferId, long chunkNum, byte[] chunkData) {
		super(sender, CoreMessageType.FX_CHUNK.toString());
		
		this.xferId = xferId;
		this.chunkNum = chunkNum;
		this.chunkData = chunkData;
		
		crc = computeCrc();
	}

	public String getXferId() {
		return xferId;
	}

	public long getChunkNum() {
		return chunkNum;
	}

	public byte[] getChunkData() {
		return chunkData;
	}

	public long getCrc() {
		return crc;
	}
	
	/**
	 * <p>Compute checksum for the current value of chunkData.</p>
	 */
	public long computeCrc() {
		CRC32 crcCalc = new CRC32();
		crcCalc.update(chunkData);
		return crcCalc.getValue();
	}
	
	/**
	 * <p>Computes checksum for the value of chunkBuf and compares it to the value stored in this instance.</p>
	 *
	 * @return true if the checksums match, otherwise false
	 */
	public boolean compareCrc() {
		return (computeCrc() == crc);
	}
}
