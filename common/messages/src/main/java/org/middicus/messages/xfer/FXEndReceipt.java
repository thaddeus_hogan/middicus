package org.middicus.messages.xfer;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class FXEndReceipt extends DSCMessage {

	public static final long serialVersionUID = 1l;
	
	private String xferId;
	private boolean error;
	private String message;
	
	public FXEndReceipt(String sender, String xferId, boolean error, String message) {
		super(sender, CoreMessageType.FX_END_RECEIPT.toString());
		this.xferId = xferId;
		this.error = error;
		this.message = message;
	}
	
	public String getXferId() {
		return xferId;
	}
	
	public boolean isError() {
		return error;
	}
	
	public String getMessage() {
		return message;
	}
}
