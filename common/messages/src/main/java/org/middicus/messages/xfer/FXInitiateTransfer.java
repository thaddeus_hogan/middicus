package org.middicus.messages.xfer;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class FXInitiateTransfer extends DSCMessage {

	public static final long serialVersionUID = 1l;
	
	private String xferId;
	private String destination;
	private String sourcePath;
	private String targetPath;
	private boolean forwardToAgent;
	
	public FXInitiateTransfer(String sender, String xferId, String destination, String sourcePath, String targetPath, boolean forwardToAgent) {
		super(sender, CoreMessageType.FX_INITIATE_TRANSFER.toString());
		this.xferId = xferId;
		this.destination = destination;
		this.sourcePath = sourcePath;
		this.targetPath = targetPath;
		this.forwardToAgent = forwardToAgent;
	}

	public String getXferId() {
		return xferId;
	}
	
	public String getDestination() {
		return destination;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public boolean getForwardToAgent() {
		return forwardToAgent;
	}
}
