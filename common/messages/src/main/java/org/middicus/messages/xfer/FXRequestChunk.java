package org.middicus.messages.xfer;

import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;

public class FXRequestChunk extends DSCMessage {

	public static final long serialVersionUID = 1l;
	
	private String xferId;
	private long chunkNum;
	
	public FXRequestChunk(String sender, String xferId, long chunkNum) {
		super(sender, CoreMessageType.FX_REQUEST_CHUNK.toString());
		this.xferId = xferId;
		this.chunkNum = chunkNum;
	}
	
	public String getXferId() {
		return xferId;
	}
	
	public long getChunkNum() {
		return chunkNum;
	}
}
