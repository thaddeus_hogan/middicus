package org.middicus.xfer;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;

public class FileTransfer {

	public enum Role {
		READING, WRITING;
	}
	
	private String xferId;
	private Role role;
	private String filePath;
	private int chunkSizeBytes;
	
	private ByteBuffer chunkBuf;
	private File file;
	private long fileLength;
	private long fileLastModified;
	
	private String destination;
	
	private PostReadHandler postReadHandler;
	private PostWriteHandler postWriteHandler;
	
	private FileChannel fch;
	
	private long lastActivityTime;
	private long lastBPS;
	
	public FileTransfer(String xferId, Role role, String filePath, int chunkSizeBytes) {
		this.xferId = xferId;
		this.role = role;
		this.filePath = filePath;
		this.chunkSizeBytes = chunkSizeBytes;

		destination = null;
		
		chunkBuf = null;
		lastBPS = 0;
		
		postReadHandler = null;
		postWriteHandler = null;
		fch = null;
	}

	public PostReadHandler getPostReadHandler() {
		return postReadHandler;
	}

	public void setPostReadHandler(PostReadHandler postReadHandler) {
		this.postReadHandler = postReadHandler;
	}

	public PostWriteHandler getPostWriteHandler() {
		return postWriteHandler;
	}

	public void setPostWriteHandler(PostWriteHandler postWriteHandler) {
		this.postWriteHandler = postWriteHandler;
	}
	
	public String getXferId() {
		return xferId;
	}
	
	public long getLastBPS() {
		return lastBPS;
	}
	
	public long getLastActivityTime() {
		return lastActivityTime;
	}
	
	public String getDestination() {
		return destination;
	}
	
	/**
	 * <p>Required for READING transfers, this value will be passed to the PostReadHandler directly.</p>
	 * @param destination A String representing the destination for read chunks
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	/**
	 * <p>Initialize the file transfer. This will open the needed file for the transfer and
	 * prepare for reading or writing as specified by this FileTransfer object's role.</p>
	 * 
	 * @throws IOException If init fails or IO library throws an IOException
	 */
	public void init() throws IOException {
		file = new File(filePath);
		
		// Setting up to READ a file, for sending data to a recipient
		if (role == Role.READING) {
			if (postReadHandler == null) { throw new IOException("postReadHandler must be set before calling FileTransfer.init()"); }
			if (destination == null) { throw new IOException("Initizalized FileTransfer with READING role and no destination set"); }
			
			if (file.exists() == false) { throw new IOException("File does not exist for reading: " + filePath); }
			fileLength = file.length();
			fileLastModified = file.lastModified();
			
			chunkBuf = ByteBuffer.allocate(chunkSizeBytes);
			fch = FileChannel.open(file.toPath(), StandardOpenOption.READ);
		}
		
		// Setting up to WRITE a file, with chunks from remote sender
		else if (role == Role.WRITING) {
			if (postWriteHandler == null) { throw new IOException("postWriteHandler must be set before calling FileTransfer.init()"); }
			fch = FileChannel.open(file.toPath(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.SPARSE);
		}
		
		lastActivityTime = System.currentTimeMillis();
	}
	
	/**
	 * <p>Reads a chunk. The result of this action will be a call to the PostReadHandler.</p>
	 * @param chunkNum Chunk number to read
	 */
	public void readChunk(long chunkNum) {
		// FIXME - Make sure this object hasn't been closed
		
		long timeSinceLastActivity = System.currentTimeMillis() - lastActivityTime;
		lastBPS = chunkSizeBytes / timeSinceLastActivity;
		
		lastActivityTime = System.currentTimeMillis();
		
		long chunkOff = chunkNum * chunkSizeBytes;
		
		// Make sure the file was not modified since the last read
		if (file.lastModified() != fileLastModified || file.length() != fileLength) {
			postReadHandler.onError(xferId, new IOException("File appears to have been modified while reading: " + file.getAbsolutePath()), destination);
			return;
		}
		
		// Make sure the requested chunk is not past the end of the file
		if (chunkOff >= fileLength) {
			postReadHandler.onEOF(xferId, destination);
			return;
		}
		
		try {
			fch.position(chunkOff);
			chunkBuf.clear();
			int bread = fch.read(chunkBuf);
			
			if (bread <= 0) {
				postReadHandler.onEOF(xferId, destination);
				return;
			}
			
			// We read some bytes successfully, let the postReadHandler know and provide a compacted byte array
			byte[] readBytes = new byte[bread];
			chunkBuf.position(0);
			chunkBuf.get(readBytes, 0, bread);
			
			postReadHandler.onSuccess(xferId, chunkNum, readBytes, destination);
		} catch (IOException ioe) {
			postReadHandler.onError(xferId, ioe, destination);
		}
	}
	
	/**
	 * <p>Writes a chunk. The result of this action will be a call to the PostWriteHandler</p>
	 * @param chunkNum Chunk number to write
	 * @param writeBytes Bytes to write to the chunk
	 */
	public void writeChunk(long chunkNum, byte[] writeBytes) {
		// FIXME - Make sure this object hasn't been closed
		
		long timeSinceLastActivity = System.currentTimeMillis() - lastActivityTime;
		lastBPS = chunkSizeBytes / timeSinceLastActivity;
		
		lastActivityTime = System.currentTimeMillis();
		
		long chunkOff = chunkNum * chunkSizeBytes;
		
		try {
			fch.position(chunkOff);
			int bwrote = fch.write(ByteBuffer.wrap(writeBytes));
			
			if (bwrote != writeBytes.length) {
				postWriteHandler.onError(xferId, new IOException("Buffer contained " + Integer.toString(writeBytes.length) + " but fewer bytes were written to the file: " + Integer.toString(bwrote)));
				return;
			}
			
			postWriteHandler.onSuccess(xferId, chunkNum);
		} catch (IOException ioe) {
			postWriteHandler.onError(xferId, ioe);
		}
	}
	
	/**
	 * <p>Call to indicate that this FileTransfer has timed out. Will call onError() method on PostReadHandler
	 * or PostWriteHandler depending on role, then close.</p>
	 * 
	 * <p>A subsequent call to close() is not necessary.</p>
	 */
	public void timeout() {
		if (role == Role.READING) { postReadHandler.onError(xferId, new IOException("FileTransfer timed out"), destination); }
		else if (role == Role.WRITING) { postWriteHandler.onError(xferId, new IOException("FileTransfer timed out")); }
		close();
	}
	
	/**
	 * <p>Closes this FileTransfer and backing files.</p>
	 */
	public void close() {
		try { fch.close(); }
		catch (IOException ioe) { }
	}
}
