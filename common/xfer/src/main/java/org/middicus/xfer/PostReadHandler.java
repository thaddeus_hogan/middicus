package org.middicus.xfer;

public interface PostReadHandler {

	public void onSuccess(String xferId, long chunkNum, byte[] readBytes, String destination);
	
	public void onError(String xferId, Exception e, String destination);
	
	public void onEOF(String xferId, String destination);
}
