package org.middicus.xfer;

public interface PostWriteHandler {

	public void onSuccess(String xferId, long chunkNum);
	
	public void onError(String xferId, Exception e);
}
