package org.middicus.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.apache.log4j.Logger;

@Singleton
@Startup
public class Config {
	
	@Inject @Middicus private Logger log;
	private Properties config = new Properties();
	
	@PostConstruct
	public void init() {
		String controllerConfigPath = System.getProperty("dsc.controller.config", null);
		if (controllerConfigPath == null) {
			log.error("System property dsc.controller.config is not set, cannot load controller config");
		} else {
			FileInputStream fis = null;
			
			try {
				File controllerConfigFile = new File(controllerConfigPath);
				fis = new FileInputStream(controllerConfigFile);
				config.load(fis);
			} catch (IOException ioe) {
				log.error("Could not load controller config file: " + controllerConfigPath, ioe);
			} finally {
				if (fis != null) { try { fis.close(); } catch (IOException ioe) { } }
			}
		}
	}
	
	@Produces @Middicus
	public Properties getConfig() {
		return config;
	}
}
