package org.middicus.engine;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.jms.ConnectionFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

public class Resources {

	@SuppressWarnings("unused")
	@Produces @Resource(mappedName = "java:/ConnectionFactory") private ConnectionFactory cf;
	
	@Produces @Middicus
	public Logger createLogger(InjectionPoint ip) {
		return Logger.getLogger(ip.getMember().getDeclaringClass());
	}
	
	@SuppressWarnings("unused")
	@Produces
	@Middicus
	@PersistenceContext(unitName = "dsc")
	private EntityManager em;
}
