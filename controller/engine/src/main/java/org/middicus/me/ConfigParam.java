package org.middicus.me;

public enum ConfigParam {
	AgentPinger_SECOND("AgentPinger.second", "How often to ping registered agents, seconds field, cron format"),
	AgentPinger_MINUTE("AgentPinger.minute", "How often to ping registered agents, minutes field, cron format"),
	AgentPinger_HOUR("AgentPinger.hour", "How often to ping registered agents, hours field, cron format"),
	
	RegistrationGroomer_SECOND("RegistrationGroomer.second", "How often to scan for expired registrations, seconds field, cron format"),
	RegistrationGroomer_MINUTE("RegistrationGroomer.minute", "How often to scan for expired registrations, minutes field, cron format"),
	RegistrationGroomer_HOUR("RegistrationGroomer.hour", "How often to scan for expired registrations, hours field, cron format"),
	
	RegistrationStore_TIMEOUT("RegistrationStore.timeout.seconds", "Agent registration expiry time in seconds"),
	
	TransferStore_TIMEOUT("TransferStore.timeout.millis", "How long (in millis) that a FileTransfer can exist witout read or write activity before it is considered dead."),
	TransferListener_CHUNK_SIZE("TransferListener.chunkSize.bytes", "Chunk size in bytes to use with file transfers");
	
	private String name;
	private String description;
	private ConfigParam(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
