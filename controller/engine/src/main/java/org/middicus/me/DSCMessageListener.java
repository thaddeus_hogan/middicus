package org.middicus.me;

import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.messages.DSCMessage;


public abstract class DSCMessageListener implements MessageListener {
	
	@Inject @Middicus private Logger superLog;
	
	@Override
	public void onMessage(Message msg) {
		try {
			DSCMessage dscMsgSuper = (DSCMessage)((ObjectMessage)msg).getObject();
			if (dscMsgSuper != null) {
				onDSCMessage(dscMsgSuper, msg);
			} else {
				superLog.error("Received ObjectMessage that contained a null object");
			}
		} catch (JMSException je) {
			superLog.error("Failed to process registration", je);
		} catch (ClassCastException cce) {
			superLog.error("Message received by DSCMessageListener was not an ObjectMessage containing a DSCMessage", cce);
		}
	}
	
	public abstract void onDSCMessage(DSCMessage msg, Message jmsMsg);
}
