package org.middicus.me;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.registration.RegistrationStore;
import org.middicus.messages.DSCMessage;


/**
 * MessageSender is a service for sending DSCMessages to agents
 */
@Singleton
public class MessageSender {

	@Inject @Middicus private Logger log;
	@Inject ConnectionFactory cf;
	@Inject RegistrationStore regStore;
	
    public MessageSender() {
    }
    
	@SuppressWarnings("unused")
	public void sendMessages(DSCMessage... dscMessages) {
    	Connection conn = null; 
    	Session sess = null;
    	
    	try {
    		conn = cf.createConnection();
    		sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
    	} catch (JMSException je) {
    		log.error("JMSException setting up for message sending", je);
    		
    		try { if (sess != null) { sess.close(); } } catch (JMSException je2) { log.warn("Closing session failed", je2); }
        	try { if (conn != null) { conn.close(); } } catch (JMSException je2) { log.warn("Closing connection failed", je2); }
    		return;
    	}
    	
    	for (DSCMessage dscMsg : dscMessages) {
    		if (dscMsg.getDestAgent() == null) {
    			log.warn("DSCMessage did not contain a destination agent");
    		} else {
    			try {
    				Queue agentQ = regStore.getQueueForAgent(dscMsg.getDestAgent());
    				
    				MessageProducer producer = sess.createProducer(agentQ);
    				
    				if (dscMsg.isDurableHint()) {
    					producer.setDeliveryMode(DeliveryMode.PERSISTENT);
    				} else {
    					producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    				}
    				
    				if (dscMsg.getTtlHint() > 0) {
    					producer.setTimeToLive(dscMsg.getTtlHint());
    				}
    				
    				ObjectMessage msg = sess.createObjectMessage(dscMsg);
    				producer.send(msg);
    				producer.close();
    			} catch (NamingException ne) {
    				log.warn("JNDI lookup for queue for agent failed: " + dscMsg.getDestAgent(), ne);
    			} catch (JMSException je) {
    				log.warn("JMSException sending message to agent: " + dscMsg.getDestAgent(), je);
    			}
    		}
    	}
    	
    	try { sess.close(); } catch (JMSException je) { log.warn("Closing session failed", je); }
    	try { conn.close(); } catch (JMSException je) { log.warn("Closing connection failed", je); }
    }
}
