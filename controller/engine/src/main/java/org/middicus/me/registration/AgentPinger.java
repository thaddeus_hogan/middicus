package org.middicus.me.registration;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.ConfigParam;
import org.middicus.me.MessageSender;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.registration.DSCPingMessage;


/**
 * Sends out periodic ping messages to all registered agents
 */
@Singleton
@Startup
public class AgentPinger {

	@Inject @Middicus private Logger log;
	@Inject private MessageSender messageSender;
	@Inject private RegistrationStore registrationStore;
	@Inject @Middicus private Properties config;
	@Resource private TimerService timerService;
	
	@PostConstruct
	public void init() {
		ScheduleExpression schedule = new ScheduleExpression();
		schedule.second(config.getProperty(ConfigParam.AgentPinger_SECOND.getName(), "0"));
		schedule.minute(config.getProperty(ConfigParam.AgentPinger_MINUTE.getName(), "*/10"));
		schedule.hour(config.getProperty(ConfigParam.AgentPinger_HOUR.getName(), "*"));
		
		log.debug("Creating timer with schedule: " + schedule.toString());
		TimerConfig tconf = new TimerConfig();
		tconf.setPersistent(false);
		timerService.createCalendarTimer(schedule, tconf);
	}
	
	@Timeout
	public void pingAgents() {
		log.debug("Pinging registered agents");
		
		Set<String> agentNames = registrationStore.getRegisteredAgentNames();
		ArrayList<DSCMessage> messages = new ArrayList<>(agentNames.size());
		
		for (String agentName : agentNames) {
			messages.add(new DSCPingMessage("controller").setDestAgent(agentName).setTtlHint(1000));
		}
		
		if (messages.size() > 0) {
			messageSender.sendMessages(messages.toArray(new DSCMessage[0]));
		}
	}
}
