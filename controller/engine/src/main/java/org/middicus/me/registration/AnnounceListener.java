package org.middicus.me.registration;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueRequestor;
import javax.jms.QueueSession;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.management.JMSManagementHelper;
import org.middicus.engine.Middicus;
import org.middicus.me.DSCMessageListener;
import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.registration.DSCPingMessage;
import org.middicus.messages.registration.DSCRegisterMessage;
import org.middicus.messages.registration.DSCRegisterResponse;
import org.middicus.messages.registration.DSCRegisterResponse.RegistrationResponse;
import org.middicus.service.agents.Agent;
import org.middicus.service.agents.AgentService;


@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/announce")
})
public class AnnounceListener extends DSCMessageListener {

	@Inject @Middicus private Logger log;
	@Resource(mappedName = "java:/ConnectionFactory") private ConnectionFactory cf;
	@Inject private RegistrationStore registrationStore;
	@Inject private AgentService agentService;
	
	@Override
	public void onDSCMessage(DSCMessage msg, Message jmsMsg) {
		// Determine type of incoming message and send to proper handler method
		if (msg.getType().equals(CoreMessageType.REGISTER.toString())) {
			try { handleRegistration((DSCRegisterMessage)msg, jmsMsg.getJMSReplyTo()); }
			catch (JMSException je) { log.error("Failed to handle registration message", je); }
		} else if (msg.getType().equals(CoreMessageType.PING.toString())) {
			try { handlePing((DSCPingMessage)msg); }
			catch (JMSException je) { log.error("Failed to handle ping message", je); }
		}
	}
	
	private void handleRegistration(DSCRegisterMessage regMsg, Destination respQ) throws JMSException {
		// See if we have a persistent record of this agent
		Agent agent = agentService.getAgent(regMsg.getSender());
		if (agent != null) {
			if (agent.getAgentHash().equals(regMsg.getAgentHash()) == false) {
				log.warn("Agent attempted to register with wrong hash: " + regMsg.getSender() + " : " + regMsg.getAgentHash());
				sendRegistrationResponse(RegistrationResponse.INVALID_AGENT_HASH, respQ, null);
				return;
			}
		} else {
			agent = new Agent();
			agent.setAgentName(regMsg.getSender());
			agent.setAgentHash(regMsg.getAgentHash());
			agentService.addAgent(agent);
		}
		
		// Determine agent queue name and JNDI binding
		String agentQueueName = "agent." + regMsg.getSender();
		String agentQueueJndi = "queue/" + agentQueueName;
		log.debug("Newly registered agent queue will be named: " + agentQueueName);
		log.debug("Newly registered agent queue JNDI will be named: " + agentQueueJndi);
		
		// Store the registration
		Registration reg = new Registration(agentQueueJndi);
		log.debug("Adding newly registered agent to the registration store");
		registrationStore.newRegistration(regMsg.getSender(), reg);
		
		// Create the agent's queue
		createQueue(agentQueueName, agentQueueJndi);
		
		// Respond to the agent
		sendRegistrationResponse(RegistrationResponse.SUCCESS, respQ, agentQueueName);
		
		log.info("Registered agent: " + regMsg.getSender());
	}
	
	private void sendRegistrationResponse(RegistrationResponse response, Destination respQ, String agentQueueName) throws JMSException {
		Connection conn = cf.createConnection();
		Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		DSCRegisterResponse resp = new DSCRegisterResponse("controller", response, agentQueueName);
		Message respMsg = sess.createObjectMessage(resp);
		MessageProducer producer = sess.createProducer(respQ);
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		producer.setTimeToLive(10000);
		producer.send(respMsg);
		
		sess.close();
		conn.close();
	}
	
	private void createQueue(String queueName, String queueJndi) throws JMSException {
		Queue mqmtQ = HornetQJMSClient.createQueue("hornetq.management");
		
		log.trace("Creating connection to management queue");
		QueueConnection conn = ((QueueConnectionFactory)cf).createQueueConnection();
		QueueSession sess = conn.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
		QueueRequestor req = new QueueRequestor(sess, mqmtQ);
		log.trace("Starting message consumption on management queue");
		conn.start();
		
		Message msg = sess.createMessage();
		log.debug("Sending message to create queue. queueName: " + queueName + " | queueJndi: " + queueJndi);
		JMSManagementHelper.putOperationInvocation(msg, "jms.server", "createQueue", queueName, queueJndi);
		Message reply = req.request(msg);
		log.trace("Reply received from management queue");
		
		try {
			log.info("Result of queue creation for queue " + queueName + ": " + JMSManagementHelper.getResult(reply).toString());
		} catch (Exception e) { log.error("Failed to retrieve result from management meessage to create queue: " + queueName); }
		
		sess.close();
		conn.close();
		
		log.info("Created agent queue " + queueName + " with JNDI name " + queueJndi);
	}
	
	private void handlePing(DSCPingMessage pingMsg) throws JMSException {
		// Update registration with recent contact time
		log.debug("Ping from " + pingMsg.getSender());
		registrationStore.touchRegistration(pingMsg.getSender());
	}

}
