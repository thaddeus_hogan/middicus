package org.middicus.me.registration;

public class Registration {
	private String queueJndi;
	private long lastPong;
	private long expectedPong;
	
	public Registration(String queueJndi) {
		this.queueJndi = queueJndi;
		lastPong = System.currentTimeMillis();
		expectedPong = 0;
	}

	public String getQueueJndi() {
		return queueJndi;
	}

	public void setQueueJndi(String queueJndi) {
		this.queueJndi = queueJndi;
	}

	public long getLastPong() {
		return lastPong;
	}

	public void setLastPong(long lastPong) {
		this.lastPong = lastPong;
	}

	public long getExpectedPong() {
		return expectedPong;
	}

	public void setExpectedPong(long expectedPong) {
		this.expectedPong = expectedPong;
	}
}
