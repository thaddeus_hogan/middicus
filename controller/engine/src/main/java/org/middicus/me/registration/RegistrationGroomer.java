package org.middicus.me.registration;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.ConfigParam;


/**
 * Periodically grooms the registration database and expires registrations for agents
 * that have not pinged within the threshold time. 
 */
@Singleton
@Startup
public class RegistrationGroomer {

	@Inject @Middicus private Logger log;
	@Inject private RegistrationStore registrationStore;
	@Inject @Middicus private Properties config;
	@Resource private TimerService timerService;
	
	@PostConstruct
	public void init() {
		ScheduleExpression schedule = new ScheduleExpression();
		schedule.second(config.getProperty(ConfigParam.RegistrationGroomer_SECOND.getName(), "0"));
		schedule.minute(config.getProperty(ConfigParam.RegistrationGroomer_MINUTE.getName(), "5,15,25,35,45,55"));
		schedule.hour(config.getProperty(ConfigParam.RegistrationGroomer_HOUR.getName(), "*"));
		
		log.debug("Creating timer with schedule: " + schedule.toString());
		TimerConfig tconf = new TimerConfig();
		tconf.setPersistent(false);
		timerService.createCalendarTimer(schedule, tconf);
	}

	@Timeout
	public void groomRegistrations() {
		log.debug("Checking registrations");
		
		long time = System.currentTimeMillis();
		
		for (String agentName : registrationStore.getRegisteredAgentNames()) {
			Registration reg = registrationStore.getRegistration(agentName);
			if (reg != null) {
				if (reg.getExpectedPong() < time) {
					log.info("Registration expired for agent: " + agentName);
					registrationStore.removeRegistration(agentName);
				}
			}
		}
	}
}
