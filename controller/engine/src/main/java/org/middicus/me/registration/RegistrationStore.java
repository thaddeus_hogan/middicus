package org.middicus.me.registration;

import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.ConfigParam;


@Singleton
public class RegistrationStore {
	
	@Inject @Middicus private Logger log;
	@Inject @Middicus private Properties config;
	
	private ConcurrentHashMap<String, Registration> registrations;
	private long regTimeoutMillis = 0;
	
	@PostConstruct
	public void init() {
		registrations = new ConcurrentHashMap<String, Registration>();
		String regTimeoutSecString = config.getProperty(ConfigParam.RegistrationStore_TIMEOUT.getName(), "600");
		try {
			regTimeoutMillis = Long.parseLong(regTimeoutSecString) * 1000;
		} catch (NumberFormatException nfe) {
			log.error("Controller config parameter was not a number: " + ConfigParam.RegistrationStore_TIMEOUT.getName(), nfe);
			regTimeoutMillis = 600000;
		}
	}
	
	public void newRegistration(String agentName, Registration registration) {
		registration.setExpectedPong(System.currentTimeMillis() + regTimeoutMillis);
		registrations.put(agentName, registration);
	}
	
	public Registration getRegistration(String agentName) {
		return registrations.get(agentName);
	}
	
	public void removeRegistration(String agentName) {
		registrations.remove(agentName);
	}
	
	public Set<String> getRegisteredAgentNames() {
		return registrations.keySet();
	}
	
	public void touchRegistration(String agentName) {
		Registration reg = registrations.get(agentName);
		if (reg != null) {
			long time = System.currentTimeMillis();
			reg.setLastPong(time);
			reg.setExpectedPong(time + regTimeoutMillis);
		}
	}
	
	/**
	 * <p>Returns a copy of the registrations database. This can be relatively slow to generate,
	 * use with care.</p>
	 */
	public HashMap<String, Registration> getRegistrationsCopy() {
		HashMap<String, Registration> regsCopy = new HashMap<>(registrations.size());
		for (String agent : registrations.keySet()) {
			Registration reg = registrations.get(agent);
			if (reg != null) { regsCopy.put(agent, reg); }
		}
		
		return regsCopy;
	}
	
	public Queue getQueueForAgent(String agentName) throws NamingException {
		Registration reg = registrations.get(agentName);
		if (reg == null) {
			throw new NamingException("No agent registered with name: " + agentName);
		} else {
			return (Queue)InitialContext.doLookup(reg.getQueueJndi());
		}
	}
}
