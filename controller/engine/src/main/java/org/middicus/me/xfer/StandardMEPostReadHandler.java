package org.middicus.me.xfer;

import javax.ejb.Singleton;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.MessageSender;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.xfer.FXChunkMessage;
import org.middicus.messages.xfer.FXEndReceipt;
import org.middicus.xfer.PostReadHandler;


@Singleton
@StandardPostReadHandler
public class StandardMEPostReadHandler implements PostReadHandler {

	@Inject @Middicus private Logger log;
	@Inject private MessageSender messageSender;
	@Inject private TransferStore transferStore;
	
	@Override
	public void onSuccess(String xferId, long chunkNum, byte[] readBytes, String destination) {
		// Send read chunk to agent
		DSCMessage msg = new FXChunkMessage("controller", xferId, chunkNum, readBytes)
			.setDurableHint(false)
			.setTtlHint(60000)
			.setDestAgent(destination);

		messageSender.sendMessages(msg);
	}

	@Override
	public void onError(String xferId, Exception e, String destination) {
		log.error("File transfer " + xferId + " ended in error", e);
		FXEndReceipt msg = new FXEndReceipt("controller", xferId, true, e.getMessage());
		msg.setDestAgent(destination);
		messageSender.sendMessages(msg);
		transferStore.endTransfer(xferId);
	}

	@Override
	public void onEOF(String xferId, String destination) {
		FXEndReceipt msg = new FXEndReceipt("controller", xferId, false, "No more chunks, transfer successful");
		msg.setDestAgent(destination);
		messageSender.sendMessages(msg);
		transferStore.endTransfer(xferId);
	}

}
