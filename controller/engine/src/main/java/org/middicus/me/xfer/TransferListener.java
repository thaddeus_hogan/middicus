package org.middicus.me.xfer;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.ConfigParam;
import org.middicus.me.DSCMessageListener;
import org.middicus.me.MessageSender;
import org.middicus.messages.CoreMessageType;
import org.middicus.messages.DSCMessage;
import org.middicus.messages.xfer.FXBeginReceipt;
import org.middicus.messages.xfer.FXEndReceipt;
import org.middicus.messages.xfer.FXInitiateTransfer;
import org.middicus.messages.xfer.FXRequestChunk;
import org.middicus.xfer.FileTransfer;
import org.middicus.xfer.FileTransfer.Role;
import org.middicus.xfer.PostReadHandler;


@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/xfer")	
})
public class TransferListener extends DSCMessageListener {

	@Inject @Middicus private Logger log;
	@Inject @Middicus private Properties config;
	@Inject private MessageSender messageSender;
	@Inject private TransferStore transferStore;
	@Inject @StandardPostReadHandler private PostReadHandler postReadHandler;
	
	@Override
	public void onDSCMessage(DSCMessage msg, Message jmsMsg) {
		if (msg.getType().equals(CoreMessageType.FX_INITIATE_TRANSFER.toString())) {
			handleInitiateTransfer((FXInitiateTransfer)msg);
		} else if (msg.getType().equals(CoreMessageType.FX_REQUEST_CHUNK.toString())) {
			handleRequestChunk((FXRequestChunk)msg);
		} else if (msg.getType().equals(CoreMessageType.FX_END_RECEIPT.toString())) {
			handleEndReceipt((FXEndReceipt)msg);
		}
	}

	/**
	 * <p>Instruction to initiate a new FileTransfer</p>
	 */
	private void handleInitiateTransfer(FXInitiateTransfer msg) {
		if (msg.getForwardToAgent() == false) {
			// NOT forwarding initiation to agent, we are sending a file from controller to agent
			int chunkSizeBytes = Integer.parseInt(config.getProperty(ConfigParam.TransferListener_CHUNK_SIZE.getName(), "65536"));
			FileTransfer xfer = new FileTransfer(msg.getXferId(), Role.READING, msg.getSourcePath(), chunkSizeBytes);
			xfer.setPostReadHandler(postReadHandler);
			xfer.setDestination(msg.getDestination());
			
			// Init transfer and return if it fails
			try {
				xfer.init();
				transferStore.addTransfer(xfer);
			} catch (IOException ioe) {
				log.error("FileTransfer.init() failed with IOException", ioe);
				return;
			}
			
			// Transfer initialized, sent agent BeginReceipt message
			FXBeginReceipt beginMsg = new FXBeginReceipt("controller", xfer.getXferId(), msg.getTargetPath(), chunkSizeBytes);
			beginMsg.setDestAgent(msg.getDestination());
			messageSender.sendMessages(beginMsg);
		} else {
			// FORWARDING initiation to agent, we are pulling a file from agent to controller
			log.error("Not implemented: forwardToAgent = true");
		}
		
	}
	
	/**
	 * <p>Recipient has requested a chunk from a FileTransfer.</p>
	 */
	private void handleRequestChunk(FXRequestChunk msg) {
		FileTransfer xfer = transferStore.getTransfer(msg.getXferId());
		if (xfer != null) {
			xfer.readChunk(msg.getChunkNum());
		} else {
			log.error("Chunk requested for non-existent FileTransfer: " + msg.getXferId());
		}
	}
	
	/**
	 * <p>Recipient is notifying us that the file transfer has ended on their end.</p>
	 * @param msg
	 */
	private void handleEndReceipt(FXEndReceipt msg) {
		if (msg.isError()) {
			log.error("FileTransfer " + msg.getXferId() + " reported in error by recipient: " + msg.getMessage());
		} else {
			log.warn("FileTransfer " + msg.getXferId() + " reported successful by recipient (it shouldn't be doing this): " + msg.getMessage());
		}
		
		transferStore.endTransfer(msg.getXferId());
	}
}
