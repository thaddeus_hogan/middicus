package org.middicus.me.xfer;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.me.ConfigParam;
import org.middicus.xfer.FileTransfer;


/**
 * <p>Stores active FileTransfer objects</p>
 */
@Singleton
public class TransferStore {

	@Inject @Middicus private Logger log;
	@Inject @Middicus private Properties config;
	
	private ConcurrentHashMap<String, FileTransfer> transfers;
	private int fileTransferTimeoutMillis = 60000;
    
    @PostConstruct
    public void init() {
    	transfers = new ConcurrentHashMap<>();
    	fileTransferTimeoutMillis = Integer.parseInt(config.getProperty(ConfigParam.TransferStore_TIMEOUT.getName(), "60000"));
    }
    
    public void addTransfer(FileTransfer xfer) {
    	transfers.put(xfer.getXferId(), xfer);
    }
    
    public void endTransfer(String xferId) {
    	FileTransfer xfer = transfers.get(xferId);
    	if (xfer != null) {
    		transfers.remove(xferId);
    		xfer.close();
    	} else {
    		log.warn("Asked to end FileTransfer that did not exist.");
    	}
    }
    
    public FileTransfer getTransfer(String xferId) {
    	return transfers.get(xferId);
    }
    
    @Schedule(hour = "*", minute = "*", second = "*/10")
    public void timeoutScan() {
    	log.trace("Scanning for timed out FileTransfers");
    	for (String xferId : transfers.keySet()) {
    		FileTransfer xfer = transfers.get(xferId);
    		if (xfer != null) {
    			if (xfer.getLastActivityTime() <= (System.currentTimeMillis() - fileTransferTimeoutMillis)) {
    				transfers.remove(xferId);
    				xfer.timeout();
    			}
    		}
    	}
    }

}
