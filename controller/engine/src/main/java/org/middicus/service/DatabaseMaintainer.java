package org.middicus.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;

/**
 * Session Bean implementation class DatabaseMaintainer
 */
@Singleton
@Startup
public class DatabaseMaintainer {

	public static final Integer minVersionForUpgrade = 1;
	public static final String SCRIPT_PKG_PREFIX = "/org/middicus/service/dbscripts/";
	
	@Inject @Middicus private Logger log;
	@Resource(mappedName = "java:jboss/datasources/MiddicusDS") private DataSource ds;
	
	/**
	 * <p>On startup, examine the configured database. Ensure there is a table containing the
	 * schema version number and then load any scripts packaged with the EJB JAR that are
	 * newer than the current schema.</p>
	 * 
	 * <p>This bean's startup will KILL THE VM if the database version cannot be reliably determined or
	 * if the database cannot be put into a known state. Processing in this bean should favor fail fast
	 * over risking further corruption to a database that might be repairable. If in doubt, terminate the VM.</p>
	 */
    @PostConstruct
    public void loadDatabase() {
    	Connection conn = null;
    	
    	try {
    		// Check for existence of the DSCDBINFO table, which tells us what version the schema is
    		conn = ds.getConnection();
    		log.debug("Looking for DSCDBINFO table");
    		ResultSet tables = conn.getMetaData().getTables(null, null, "DSCDBINFO", null);
    		boolean dbInfoExists = tables.next();
    		tables.close();
    		
    		// If the table does not exist, create it, version will be set to 0
    		if (dbInfoExists == false) {
    			log.info("Creating DSCDBINFO table, this database will be reloaded from scratch");
    			loadDbInfoTable(conn);
    		}
    		
    		Integer dbVersion = getDbVersion(conn);
    		
    		// Load database script newer than the current database version, and base if version is 0
    		log.debug("DSC database at version " + dbVersion);
    		loadScripts(dbVersion, conn);
    		
    	} catch (SQLException sqle) {
    		log.error("Error accessing the database for initial load", sqle);
    		System.exit(1);
    	} finally {
    		if (conn != null) { try { conn.close(); } catch (SQLException sqle) { } }
    	}
    	
    }
    
    private void loadDbInfoTable(Connection conn) throws SQLException {
    	String sql = "CREATE TABLE DSCDBINFO ("
    		+ "NAME VARCHAR(255) NOT NULL PRIMARY KEY,"
    		+ "VALUE VARCHAR(255) NOT NULL)";
    	Statement stmt = conn.createStatement();
    	stmt.execute(sql);
    	stmt.execute("INSERT INTO DSCDBINFO (NAME, VALUE) VALUES ('VERSION', '0')");
    	stmt.close();
    	log.debug("DSCDBINFO table created");
    }

    private void loadScripts(Integer fromVersion, Connection conn) throws SQLException {
    	// Load the base script if version is 0
    	if (fromVersion == 0) {
    		log.info("Loading base SQL script");
    		String resourcePath = SCRIPT_PKG_PREFIX + "base.sql";
    		InputStream stream = getClass().getClassLoader().getResourceAsStream(resourcePath);
    		if (stream == null) {
    			log.fatal("Could not open base SQL resource: " + resourcePath);
    			System.exit(1);
    		}
    		
    		try {
    			loadScriptFromStream(stream, conn);
    		} catch (SQLException sqle) {
    			throw sqle;
    		} catch (IOException ioe) {
    			try { if (stream != null) { stream.close(); } } catch (IOException ioe2) { }
    		}
    		
    		// The base script will have brought the DB up to some version, check it
    		fromVersion = getDbVersion(conn);
    	}
    	
    	// Load all scripts until by incrementing script number until no script is found
    	int script = fromVersion + 1;
    	boolean scriptFound = true;
    	
    	String versionSql = "UPDATE DSCDBINFO AS I SET I.VALUE = ? WHERE I.NAME = 'VERSION'";
		PreparedStatement versionUpdate = conn.prepareStatement(versionSql);
		
    	while (scriptFound) {
    		String resourcePath = SCRIPT_PKG_PREFIX + "version-" + Integer.toString(script) + ".sql";
    		InputStream stream = getClass().getClassLoader().getResourceAsStream(resourcePath);
    		if (stream == null) {
    			scriptFound = false;
    		} else {
    			try {
        			loadScriptFromStream(stream, conn);
        			
        			versionUpdate.setString(1, Integer.toString(script));
        			versionUpdate.execute();
        		} catch (SQLException sqle) {
        			throw sqle;
        		} catch (IOException ioe) {
        			try { if (stream != null) { stream.close(); } } catch (IOException ioe2) { }
        		}
    			
    			script += 1;
    		}
    	}
    	
    	versionUpdate.close();
    }
    
    private void loadScriptFromStream(InputStream stream, Connection conn) throws SQLException, IOException {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    	
    	String line = "";
    	String sqlStatement = "";
    	Statement stmt = conn.createStatement();
    	
    	while ((line = reader.readLine()) != null) {
    		line = line.trim();
    		
    		if (line.length() == 0) { continue; }
    		if (line.startsWith("--") == true) { continue; }
    		if (line.equals(";") == true) {
    			// Line is a ; by itself, execute whatever is built up in sqlStatement
    			log.debug("Executing statement: " + sqlStatement);
    			stmt.execute(sqlStatement);
    			sqlStatement = "";
    		} else {
    			sqlStatement += line + " ";
    		}
    	}
    	
    	stmt.close();
    }
    
    private Integer getDbVersion(Connection conn) throws SQLException {
    	// Check the schema version
		Integer dbVersion = null;
		Statement stmt = conn.createStatement();
		ResultSet versionRes = stmt.executeQuery("SELECT NAME, VALUE FROM DSCDBINFO WHERE NAME = 'VERSION'");
		if (versionRes.next() == false) {
			log.fatal("Failed to find version record in DSCDBINFO database, terminating as to not accidently overwrite data");
			System.exit(1);
		}
		
		try {
			dbVersion = Integer.parseInt(versionRes.getString("VALUE"));
		} catch (NumberFormatException nfe) {
			log.fatal("Could not determine database version, version was not a number?", nfe);
			System.exit(1);
		}
		
		versionRes.close();
		stmt.close();
		
		return dbVersion;
    }
}
