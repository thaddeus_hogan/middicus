package org.middicus.service;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.messages.DSCMessage;


/**
 * <p>Helper for sending DSCMessages from the service to the message engine</p>
 */
@Singleton
public class ServiceMessageSender {

    @Inject @Middicus private Logger log;
    @Resource(mappedName = "java:/ConnectionFactory") private ConnectionFactory cf;
    
    @SuppressWarnings("unused")
	public void sendMessage(DSCMessage msg, String destQueueJNDI) {
    	Queue destQueue = null;
    	try {
    		destQueue = (Queue)InitialContext.doLookup(destQueueJNDI);
    	} catch (NamingException ne) {
    		log.error("Failed to look up requested Queue: " + destQueueJNDI);
    	}
    	
    	Connection conn = null;
    	Session sess = null;
    	
    	try {
    		conn = cf.createConnection();
    		sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
    	} catch (JMSException je) {
    		log.error("JMSException setting up for message sending", je);
    		
    		try { if (sess != null) { sess.close(); } } catch (JMSException je2) { log.warn("Closing session failed", je2); }
        	try { if (conn != null) { conn.close(); } } catch (JMSException je2) { log.warn("Closing connection failed", je2); }
    		return;
    	}
    	
		try {
			MessageProducer producer = sess.createProducer(destQueue);
			
			if (msg.isDurableHint()) {
				producer.setDeliveryMode(DeliveryMode.PERSISTENT);
			} else {
				producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			}
			
			if (msg.getTtlHint() > 0) {
				producer.setTimeToLive(msg.getTtlHint());
			}
			
			ObjectMessage objMsg = sess.createObjectMessage(msg);
			producer.send(objMsg);
			producer.close();
		} catch (JMSException je) {
			log.warn("JMSException sending message to queue: " + destQueueJNDI, je);
		}
    	
    	try { sess.close(); } catch (JMSException je) { log.warn("Closing session failed", je); }
    	try { conn.close(); } catch (JMSException je) { log.warn("Closing connection failed", je); }
    }
}
