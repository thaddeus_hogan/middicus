package org.middicus.service.agents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Stores a persistent record of agent registrations so that changed agentHash
 * values can be flagged.
 */
@Entity
@Table(name = "AGENT")
public class Agent implements Serializable {
	private static final long serialVersionUID = 1l;
	
	private String agentName;
	private String agentHash;
	private Integer folderId;

	public Agent() {
	}

	@Id
	@Column(name = "AGENT_NAME")
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	@Column(name = "AGENT_HASH")
	@NotEmpty
	@Size(max = 36)
	public String getAgentHash() {
		return agentHash;
	}

	public void setAgentHash(String agentHash) {
		this.agentHash = agentHash;
	}

	@Column(name = "FOLDER_ID")
	public Integer getFolderId() {
		return folderId;
	}

	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}
}
