package org.middicus.service.agents;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "AGENT_FOLDER")
public class AgentFolder implements Serializable {
	public static final long serialVersionUID = 1l;
	
	private Integer id;
	private String name;
	private String icon;
	private Integer parent;
	
	private transient ArrayList<AgentFolder> children = new ArrayList<>();
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "NAME")
	@Size(max = 255)
	@NotNull @NotEmpty
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "ICON")
	@Size(max = 255)
	@NotNull @NotEmpty
	public String getIcon() {
		return icon;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public Integer getParent() {
		return parent;
	}
	
	public void setParent(Integer parent) {
		this.parent = parent;
	}

	@Transient
	public ArrayList<AgentFolder> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<AgentFolder> children) {
		this.children = children;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj instanceof AgentFolder) {
			AgentFolder other = (AgentFolder)obj;
			if (other.getId() == null) { return false; }
			return ((AgentFolder)obj).getId().equals(id);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		if (id == null) { return 1; }
		return id.hashCode();
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
