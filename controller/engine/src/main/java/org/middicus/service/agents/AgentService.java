package org.middicus.service.agents;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;


@Path("/agentService")
@Stateless
public class AgentService {
	
	@Inject @Middicus private Logger log;
	@Inject @Middicus private EntityManager em;
	
	@GET
	@Path("/getAllAgents")
	@Produces(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Agent> getAllAgents() {
		return em.createQuery("SELECT a from Agent a", Agent.class).getResultList();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Agent getAgent(String agentName) {
		log.trace("Get agent " + agentName);
		return em.find(Agent.class, agentName);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addAgent(Agent agent) {
		log.trace("Create agent " + agent.getAgentName() + " : " + agent.getAgentHash());
		em.persist(agent);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateAgent(Agent agent) {
		log.trace("Update agent " + agent.getAgentName());
		em.merge(agent);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeAgent(String agentName) {
		log.trace("Delete agent " + agentName);
		em.createQuery("DELETE from Agent a where a.agentName = :agentName", Agent.class)
			.setParameter("agentName", agentName)
			.executeUpdate();
	}
	
	@GET
	@Path("/getAllFolders")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AgentFolder> getAllFolders() {
		return em.createQuery("SELECT f FROM AgentFolder f", AgentFolder.class).getResultList();
	}
	
	@POST
	@Path("/createFolder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AgentFolder createFolder(AgentFolder folder) {
		em.persist(folder);
		return folder;
	}
	
	@POST
	@Path("/renameFolder")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void renameFolder(@FormParam("id") Integer id, @FormParam("newName") String newName) {
		em.createQuery("UPDATE AgentFolder f SET f.name = :newName WHERE f.id = :id")
			.setParameter("id", id).setParameter("newName", newName).executeUpdate();
	}
	
	@POST
	@Path("/deleteFolder")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFolder(@FormParam("id") Integer id) {
		em.createQuery("DELETE from AgentFolder f WHERE f.id = :id")
			.setParameter("id", id).executeUpdate();
	}
	
	@GET
	@Path("/getFolderClassifiers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClassifierMappingFolder> getFolderClassifiers(@QueryParam("folderId") Integer folderId) {
		TypedQuery<ClassifierMappingFolder> query = em.createQuery(
			"SELECT m FROM ClassifierMappingFolder m WHERE m.folderId = :folderId", ClassifierMappingFolder.class);
		query.setParameter("folderId", folderId);
		return query.getResultList();
	}
	
	@GET
	@Path("/getAllFolderClassifiers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClassifierMappingFolder> getAllFolderClassifiers() {
		return em.createQuery("SELECT m FROM ClassifierMappingFolder m", ClassifierMappingFolder.class).getResultList();
	}
	
	@POST
	@Path("/addFolderClassifier")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ClassifierMappingFolder addFolderClassifier(ClassifierMappingFolder mapping) {
		em.persist(mapping);
		return mapping;
	}
	
	@POST
	@Path("/deleteFolderClassifier")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFolderClassifier(@FormParam("id") Integer id)	{
		em.createQuery("DELETE FROM ClassifierMappingFolder m WHERE m.id = :id")
			.setParameter("id", id)
			.executeUpdate();
	}
	
	@GET
	@Path("/getAgentsInFolder")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Agent> getAgentsInFolder(@QueryParam("folderId") Integer folderId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Agent> query = cb.createQuery(Agent.class);
		Root<Agent> agent = query.from(Agent.class);
		query.select(agent);
		if (folderId == null) { query.where(cb.isNull(agent.get("folderId"))); }
		else { query.where(cb.equal(agent.get("folderId"), folderId)); }
		
		return em.createQuery(query).getResultList();
	}
	
	@POST
	@Path("/setAgentFolder")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void setAgentFolder(@FormParam("agentName") String agentName, @FormParam("folderId") Integer folderId) {
		Agent agent = getAgent(agentName);
		if (agent != null) {
			agent.setFolderId(folderId);
			updateAgent(agent);
		}
	}
}
