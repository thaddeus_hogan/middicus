package org.middicus.service.agents;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entity implementation class for Entity: ClassifierMapping
 *
 */
@Entity
@Table(name = "CLASSIFIER_MAPPING_AGENT")
public class ClassifierMappingAgent implements Serializable {
	
	private static final long serialVersionUID = 1l;
	
	private Integer id;
	private String classifier;
	private String agentName;
	
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	
	@Column(name = "CLASSIFIER")
	@Size(max = 255)
	@NotNull @NotEmpty
	public String getClassifier() {
		return this.classifier;
	}

	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}
	
	@Column(name = "AGENT_NAME")
	@Size(max = 255)
	@NotNull @NotEmpty
	public String getAgentName() {
		return this.agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
}
