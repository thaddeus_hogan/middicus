package org.middicus.service.agents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "CLASSIFIER_MAPPING_FOLDER")
public class ClassifierMappingFolder implements Serializable {
	public static final long serialVersionUID = 1l;
	
	private Integer id;
	private String classifier;
	private Integer folderId;
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "CLASSIFIER")
	@Size(max = 255)
	@NotNull @NotEmpty
	public String getClassifier() {
		return classifier;
	}
	
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}
	
	@Column(name = "FOLDER_ID")
	@NotNull
	public Integer getFolderId() {
		return folderId;
	}
	
	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}
}
