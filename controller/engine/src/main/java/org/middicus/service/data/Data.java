package org.middicus.service.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entity implementation class for Entity: Data
 *
 */
@Entity
@Table(name = "DATA")
public class Data implements Serializable {
	
	private static final long serialVersionUID = 1l;

	private Integer id;
	private String key;
	private String classifier;
	private Integer valueType;
	private String value;
	private String jsonDefaultsKey;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "KEY")
	@NotNull(message = "A key must be specified")
	@Size(min = 1, max = 255, message = "Key length must be between 1 and 255 characters")
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	@Column(name = "CLASSIFIER")
	public String getClassifier() {
		return classifier;
	}
	
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}
	
	@Transient
	@JsonIgnore
	public ValueType getValueType() {
		return ValueType.getFromInt(valueType);
	}
	
	public void setValueType(ValueType valueType) {
		this.valueType = valueType.getValType();
	}
	
	@Column(name = "VALUE_TYPE")
	@NotNull(message = "A value type must be specified")
	public Integer getValueTypeInt() {
		return valueType;
	}
	
	public void setValueTypeInt(Integer valueType) {
		this.valueType = valueType;
	}
	
	@Column(name = "VALUE")
	@Basic(fetch = FetchType.LAZY)
	@JsonIgnore
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	@Column(name = "JSON_DEFAULTS_KEY")
	public String getJsonDefaultsKey() {
		return jsonDefaultsKey;
	}
	
	public void setJsonDefaultsKey(String jsonDefaultsKey) {
		this.jsonDefaultsKey = jsonDefaultsKey;
	}
	
	@Transient
	public String getDisplayValue() {
		if (getValueType() == ValueType.STRING) {
			return getValue();
		} else if (getValueType() == ValueType.JSON) {
			return "{JSON}";
		}
		
		return "";
	}
}
