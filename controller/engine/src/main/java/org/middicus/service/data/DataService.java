package org.middicus.service.data;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;


@Stateless
@Path("/dataService")
public class DataService {

	@Inject @Middicus private Logger log;
	@Inject @Middicus private EntityManager em;
	
	@GET
	@Path("/getDataByKeyPattern")
	@Produces(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Data> getDataByKeyPattern(@QueryParam("keyPattern") String keyPattern) {
		if (keyPattern == null || keyPattern.length() == 0) {
			return em.createQuery("SELECT d FROM Data d", Data.class).getResultList();
		}
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Data> query = cb.createQuery(Data.class);
		Root<Data> data = query.from(Data.class);
		query.select(data);
		query.where(cb.like(data.<String>get("key"), "%" + keyPattern + "%"));
		
		return em.createQuery(query).getResultList();
	}
	
	@GET
	@Path("/getValueById")
	@Produces(MediaType.TEXT_PLAIN)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getValueById(@QueryParam("id") Integer id) {
		Data d = em.createQuery("SELECT d FROM Data d WHERE d.id = :id", Data.class)
			.setParameter("id", id).getSingleResult();
		if (d == null) { return null; }
		else { return d.getValue(); }
	}
	
	@POST
	@Path("/createData")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Data createData(Data data) {
		if (data.getValue() == null && data.getValueType() == ValueType.STRING) { data.setValue(""); }
		else if (data.getValue() == null && data.getValueType() == ValueType.JSON) { data.setValue("{}"); }
		em.persist(data);
		return data;
	}
	
	@POST
	@Path("/updateData")
	@Consumes(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateData(Data data) {
		String queryStr = "UPDATE Data d " +
			"SET d.key = :key, d.classifier = :classifier, " +
			"d.jsonDefaultsKey = :jsonDefaultsKey, d.valueTypeInt = :valueTypeInt " +
			"WHERE d.id = :id";
		em.createQuery(queryStr)
			.setParameter("id", data.getId())
			.setParameter("key", data.getKey())
			.setParameter("classifier", data.getClassifier())
			.setParameter("jsonDefaultsKey", data.getJsonDefaultsKey())
			.setParameter("valueTypeInt", data.getValueTypeInt())
			.executeUpdate();
	}
	
	@POST
	@Path("/saveValue")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveValue(@FormParam("id") Integer id, @FormParam("value") String value) {
		em.createQuery("UPDATE Data d SET d.value = :value WHERE d.id = :id")
			.setParameter("id", id)
			.setParameter("value", value)
			.executeUpdate();
	}
	
	@POST
	@Path("/deleteDataByIds")
	@Consumes(MediaType.APPLICATION_JSON)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteDataByIds(ArrayList<Integer> ids) {
		Query query = em.createQuery("DELETE FROM Data d WHERE d.id = :id");
		for (Integer id : ids) {
			query.setParameter("id", id);
			query.executeUpdate();
		}
	}
}
