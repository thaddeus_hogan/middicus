package org.middicus.service.data;

public enum ValueType {
	STRING(1),
	JSON(2);
	
	private int valType;
	
	private ValueType(int valType) {
		this.valType = valType;
	}
	
	public int getValType() { return valType; }
	
	public static ValueType getFromInt(int valType) {
		for (ValueType t : ValueType.values()) {
			if (t.getValType() == valType) { return t; }
		}
		return null;
	}
}
