-- Statements are terminated by a semicolon (;) on its own line
-- Semicolons with any other characters around them are ignored
-- This is so that the data loader does not have to fully parse the SQL

-- DSC Database Base SQL Script - version 3

CREATE TABLE AGENT_FOLDER (
	ID IDENTITY,
	NAME VARCHAR(255) NOT NULL,
	ICON VARCHAR(255) NOT NULL,
	PARENT INTEGER
)
;

ALTER TABLE AGENT_FOLDER
	ADD CONSTRAINT FK_AGENT_FOLDER_PARENT
	FOREIGN KEY (PARENT) REFERENCES AGENT_FOLDER (ID) ON DELETE CASCADE
;

CREATE TABLE AGENT (
	AGENT_NAME VARCHAR(255) NOT NULL PRIMARY KEY,
	AGENT_HASH VARCHAR(36) NOT NULL,
	FOLDER_ID INTEGER
)
;

ALTER TABLE AGENT
	ADD CONSTRAINT FK_AGENT_FOLDER_ID
	FOREIGN KEY (FOLDER_ID) REFERENCES AGENT_FOLDER (ID) ON DELETE SET NULL
;

CREATE TABLE CLASSIFIER_MAPPING_AGENT (
	ID IDENTITY,
	CLASSIFIER VARCHAR(255) NOT NULL,
	AGENT_NAME VARCHAR(255) NOT NULL
)
;

ALTER TABLE CLASSIFIER_MAPPING_AGENT
	ADD CONSTRAINT FK_CLASSIFIER_MAPPING_AGENT_AGENT_NAME
	FOREIGN KEY (AGENT_NAME) REFERENCES AGENT (AGENT_NAME) ON DELETE CASCADE
;

CREATE TABLE CLASSIFIER_MAPPING_FOLDER (
	ID IDENTITY,
	CLASSIFIER VARCHAR(255) NOT NULL,
	FOLDER_ID INTEGER NOT NULL
)
;

ALTER TABLE CLASSIFIER_MAPPING_FOLDER
	ADD CONSTRAINT FK_CLASSIFIER_MAPPING_FOLDER_FOLDER_ID
	FOREIGN KEY (FOLDER_ID) REFERENCES AGENT_FOLDER (ID) ON DELETE CASCADE
;

UPDATE DSCDBINFO AS I SET I.VALUE = '3' WHERE I.NAME = 'VERSION'
;