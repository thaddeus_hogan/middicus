-- VALUE column of DATA table should not be nullable

ALTER TABLE DATA ALTER COLUMN VALUE SET NOT NULL
;