package org.middicus.service.xfer;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;
import org.middicus.messages.xfer.FXInitiateTransfer;
import org.middicus.service.ServiceMessageSender;


@Path("/transferService")
@Stateless
public class TransferService {
	
	@Inject @Middicus private Logger log;
	@Inject private ServiceMessageSender messageSender;

	@POST
	@Path("/transferToAgent")
	public void transferToAgent(@FormParam("agent") String agent, @FormParam("sourcePath") String sourcePath, @FormParam("targetPath") String targetPath) {
		log.info("Initiate Transfer of " + sourcePath + " to " + agent + ":" + targetPath);
		
		String xferId = UUID.randomUUID().toString();
		FXInitiateTransfer msg = new FXInitiateTransfer("transferService", xferId, agent, sourcePath, targetPath, false);
		
		messageSender.sendMessage(msg, "queue/xfer");
	}
}
