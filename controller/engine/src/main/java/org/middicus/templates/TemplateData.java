package org.middicus.templates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TEMPLATE_DATA")
public class TemplateData {
	
	private Integer id;
	private String templateData;
	private String notes;
	
	public TemplateData() {
		templateData = "";
		notes = "";
	}

	@Id
	@Column(name = "ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TEMPLATE_DATA")
	@NotNull
	public String getTemplateData() {
		return templateData;
	}

	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}

	@Column(name = "NOTES")
	@NotNull
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
