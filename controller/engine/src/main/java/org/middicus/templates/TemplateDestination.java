package org.middicus.templates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TEMPLATE_DESTINATION")
public class TemplateDestination {

	private Integer id;
	private Integer templateId;
	private String classifierExpression;
	private String targetPath;
	
	public TemplateDestination() {
		classifierExpression = "";
		targetPath = "";
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "TEMPLATE_ID")
	@NotNull
	public Integer getTemplateId() {
		return templateId;
	}
	
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	
	@Column(name = "CLASSIFIER_EXPRESSION")
	@NotNull
	public String getClassifierExpression() {
		return classifierExpression;
	}
	
	public void setClassifierExpression(String classifierExpression) {
		this.classifierExpression = classifierExpression;
	}

	@Column(name = "TARGET_PATH")
	@NotNull
	public String getTargetPath() {
		return targetPath;
	}
	
	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}
}
