package org.middicus.templates;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.middicus.engine.Middicus;


/**
 * Session Bean implementation class TemplateService
 */
@Stateless
@Path("/templateService")
public class TemplateService {

    @Inject @Middicus private Logger log;
    @Inject @Middicus private EntityManager em;
    
    @POST
    @Path("/createTemplate")
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Template createTemplate(Template template) {
    	em.persist(template);
    	TemplateData data = new TemplateData();
    	data.setId(template.getId());
    	em.persist(data);
    	
    	return template;
    }
    
    @POST
    @Path("/updateTemplate")
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateTemplate(Template template) {
    	em.merge(template);
    }
    
    @GET
    @Path("/getTemplateById")
    @Produces(MediaType.APPLICATION_JSON)
    public Template getTemplateById(Integer id) {
    	return em.find(Template.class, id);
    }

    @GET
    @Path("/getTemplatesByNamePattern")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Template> getTemplatesByNamePattern(String namePattern) {
    	if (namePattern == null || namePattern.length() == 0) {
    		return em.createQuery("SELECT t From Template t", Template.class).getResultList();
    	}
    	
    	CriteriaBuilder cb = em.getCriteriaBuilder();
    	
    	CriteriaQuery<Template> query = cb.createQuery(Template.class);
    	Root<Template> template = query.from(Template.class);
    	query.select(template);
    	query.where(cb.like(template.<String>get("templateName"), "%" + namePattern + "%"));
    	
    	return em.createQuery(query).getResultList();
    }
    
    @POST
    @Path("/deleteTemplate")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteTemplate(@FormParam("id") Integer id) {
    	em.createQuery("DELETE FROM TemplateDestination td WHERE td.templateId = :id")
    		.setParameter("id", id)
    		.executeUpdate();
    	em.createQuery("DELETE FROM TemplateData td WHERE td.id = :id")
    		.setParameter("id", id)
    		.executeUpdate();
    	em.createQuery("DELETE FROM Template t WHERE t.id = :id")
    		.setParameter("id", id)
    		.executeUpdate();
    }
    
    @POST
    @Path("/saveTemplateData")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void saveTemplateData(@FormParam("id") Integer id, @FormParam("data") String data) {
    	if (data == null) { data = ""; } // To deal with RESTEasy bug
    	em.createQuery("UPDATE TemplateData td SET td.templateData = :data WHERE td.id = :id")
    		.setParameter("data", data)
    		.setParameter("id", id)
    		.executeUpdate();
    }
    
    @GET
    @Path("/loadTemplateData")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String loadTemplateData(@QueryParam("id") Integer id) {
    	TemplateData data = em.find(TemplateData.class, id);
    	return data.getTemplateData();
    }
}
