package org.middicus.web.bean.agents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.middicus.service.agents.AgentFolder;
import org.primefaces.model.TreeNode;

public class AgentFolderTreeNode implements TreeNode, Serializable {
	public static final long serialVersionUID = 1l;

	private AgentFolder folder;
	private List<TreeNode> children;
	private TreeNode parent;
	
	private boolean selectable;
	private boolean selected;
	private boolean expanded;
	
	public AgentFolderTreeNode(AgentFolder folder) {
		if (folder == null) {
			this.folder = new AgentFolder();
			this.folder.setId(-1);
			this.folder.setParent(null);
			this.folder.setIcon("folder.png");
			this.folder.setName("Agent Folders");
		} else {
			this.folder = folder;
		}
		
		parent = null;
		
		selectable = true;
		selected = false;
		expanded = false;
		
		children = new ArrayList<>();
	}
	
	protected void populateChildren(List<AgentFolder> folders) {
		for (AgentFolder f : folders) {
			if (f.getParent() == folder.getId()) {
				AgentFolderTreeNode node = new AgentFolderTreeNode(f);
				node.setParent(this);
				children.add(node);
				
				node.populateChildren(folders);
			}
		}
	}
	
	@Override
	public int getChildCount() {
		return children.size();
	}

	@Override
	public List<TreeNode> getChildren() {
		return children;
	}

	@Override
	public Object getData() {
		return folder;
	}
	
	public AgentFolder getFolder() {
		return folder;
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public String getType() {
		return folder.getIcon();
	}

	@Override
	public boolean isExpanded() {
		return expanded;
	}

	@Override
	public boolean isLeaf() {
		return children.size() == 0;
	}

	@Override
	public boolean isSelectable() {
		return selectable;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
		if (parent != null) { parent.setExpanded(expanded); }
	}

	@Override
	public void setParent(TreeNode parent) {
		this.parent = parent;
	}

	@Override
	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj instanceof AgentFolderTreeNode) {
			AgentFolderTreeNode other = (AgentFolderTreeNode)obj;
			if (other.getFolder() == null && folder == null) { return true; }
			return other.getFolder().equals(folder);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return folder == null ? 1 : folder.hashCode();
	}
	
	@Override
	public String toString() {
		if (folder != null) { return folder.getId().toString(); }
		return "";
	}

	public static AgentFolderTreeNode createTree(List<AgentFolder> folders) {
		AgentFolderTreeNode root = new AgentFolderTreeNode(null);
		AgentFolderTreeNode visRoot = new AgentFolderTreeNode(null);
		
		visRoot.setParent(root);
		root.getChildren().add(visRoot);
		visRoot.setExpanded(true);
		// visRoot.setSelected(true);
		
		for (AgentFolder f : folders) {
			if (f.getParent() == null) {
				AgentFolderTreeNode node = new AgentFolderTreeNode(f);
				node.setParent(visRoot);
				visRoot.getChildren().add(node);
				
				node.populateChildren(folders);
			}
		}
		
		return root;
	}
}
