package org.middicus.web.bean.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.middicus.me.registration.Registration;
import org.middicus.me.registration.RegistrationStore;
import org.middicus.service.agents.Agent;
import org.middicus.service.agents.AgentService;


@Named("agentList")
@RequestScoped
public class AgentList {

	@Inject private transient Logger log;
	@Inject private AgentService agentService;
	@Inject private RegistrationStore registrationStore;
	
	public AgentList() {
		// TODO Auto-generated constructor stub
	}

	public List<AgentTableItem> getList() {
		ArrayList<AgentTableItem> items = new ArrayList<>();
		List<Agent> agents = agentService.getAllAgents();
		HashMap<String, Registration> regs = registrationStore.getRegistrationsCopy();

		for (Agent agent : agents) {
			AgentTableItem item = new AgentTableItem();
			item.setAgent(agent);
			if (regs.keySet().contains(agent.getAgentName())) {
				item.setRegistered(true);
			} else {
				item.setRegistered(false);
			}
			
			items.add(item);
		}
		
		return items;
	}
}
