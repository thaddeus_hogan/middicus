package org.middicus.web.bean.agents;

import org.middicus.service.agents.Agent;

public class AgentTableItem {
	
	private Agent agent;
	private Boolean registered;
	
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	public Boolean getRegistered() {
		return registered;
	}
	public void setRegistered(Boolean registered) {
		this.registered = registered;
	}
	
}
