package org.middicus.web.bean.agents;

import java.io.Serializable;

public class FolderIconOption implements Serializable {
	public static final long serialVersionUID = 1l;

	private String name;
	private String icon;
	
	public FolderIconOption(String name, String icon) {
		this.name = name;
		this.icon = icon;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Override
	public String toString() {
		return icon;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FolderIconOption) {
			return ((FolderIconOption)obj).getIcon().equals(icon);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return icon.hashCode();
	}
}
