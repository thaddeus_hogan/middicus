package org.middicus.web.bean.agents;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("FolderIconOptionConverter")
@RequestScoped
public class FolderIconOptionConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		return new FolderIconOption(value, value);
	}
	
	@Override
	public String getAsString(FacesContext ctx, UIComponent component, Object value) {
		return value.toString();
	}
}
