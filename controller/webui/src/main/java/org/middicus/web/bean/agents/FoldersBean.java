package org.middicus.web.bean.agents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.middicus.engine.Middicus;
import org.middicus.service.agents.Agent;
import org.middicus.service.agents.AgentFolder;
import org.middicus.service.agents.AgentService;
import org.middicus.service.agents.ClassifierMappingFolder;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;

@ViewScoped
@Named("folders")
public class FoldersBean implements Serializable {
	public static final long serialVersionUID = 1l;

	@Inject @Middicus private transient Logger log;
	@Inject private transient AgentService agentService;
	
	private AgentFolderTreeNode agentFolderTree;
	private AgentFolderTreeNode selectedFolderNode;
	private List<Agent> selectedFolderAgents;
	private List<Agent> rootFolderAgents;
	
	private List<ClassifierMappingFolder> selectedFolderClassifiers;
	private String newClassifier = "";
	private Integer deleteClassifierId;
	
	private AgentFolder newFolder = new AgentFolder();
	private FolderIconOption newFolderIcon = new FolderIconOption("folder.png", "folder.png");
	private ArrayList<FolderIconOption> folderIconOptions;
	
	private String renameFolderName = "";
	
	public AgentFolderTreeNode getAgentFolderTree() { return agentFolderTree; }
	public AgentFolderTreeNode getSelectedFolderNode() { return selectedFolderNode; }
	public void setSelectedFolderNode(AgentFolderTreeNode selectedFolderNode) { this.selectedFolderNode = selectedFolderNode; }
	public List<Agent> getSelectedFolderAgents() { return selectedFolderAgents; }
	public void setSelectedFolderAgents(List<Agent> selectedFolderAgents) { this.selectedFolderAgents = selectedFolderAgents; }
	public List<Agent> getRootFolderAgents() { return rootFolderAgents; }
	public void setRootFolderAgents(List<Agent> rootFolderAgents) { this.rootFolderAgents = rootFolderAgents; }
	public AgentFolder getNewFolder() { return newFolder; }
	public void setNewFolder(AgentFolder newFolder) { this.newFolder = newFolder; }
	public FolderIconOption getNewFolderIcon() { return newFolderIcon; }
	public void setNewFolderIcon(FolderIconOption newFolderIcon) { this.newFolderIcon = newFolderIcon; }
	public ArrayList<FolderIconOption> getFolderIconOptions() { return folderIconOptions; }
	public List<ClassifierMappingFolder> getSelectedFolderClassifiers() { return selectedFolderClassifiers; }
	public void setSelectedFolderClassifiers(List<ClassifierMappingFolder> selectedFolderClassifiers) { this.selectedFolderClassifiers = selectedFolderClassifiers; }
	public String getNewClassifier() { return newClassifier; }
	public void setNewClassifier(String newClassifier) { this.newClassifier = newClassifier; }
	public Integer getDeleteClassifierId() { return deleteClassifierId; }
	public void setDeleteClassifierId(Integer deleteClassifierId) { this.deleteClassifierId = deleteClassifierId; }
	
	@NotEmpty
	public String getRenameFolderName() { return renameFolderName; }
	public void setRenameFolderName(String renameFolderName) { this.renameFolderName = renameFolderName; }
	
	@PostConstruct
	public void init() {
		// Load data
		loadTree();
		
		// Setup new folder dialog icon dropdown
		folderIconOptions = new ArrayList<>();
		folderIconOptions.add(new FolderIconOption("datacenter.png", "datacenter.png"));
		folderIconOptions.add(new FolderIconOption("network.png", "network.png"));
		folderIconOptions.add(new FolderIconOption("folder.png", "folder.png"));
	}
	
	public void onFolderSelection() {
		selectedFolderAgents = agentService.getAgentsInFolder(selectedFolderNode.getFolder().getId());
		selectedFolderClassifiers = agentService.getFolderClassifiers(selectedFolderNode.getFolder().getId());
	}
	
	public void onFolderExpand(NodeExpandEvent event) {
		event.getTreeNode().setExpanded(true);
	}
	
	public void onFolderCollapse(NodeCollapseEvent event) {
		event.getTreeNode().setExpanded(false);
	}
	
	public void onUncategorizedDrop(DragDropEvent event) {
		Agent droppedAgent = (Agent)event.getData();
		agentService.setAgentFolder(droppedAgent.getAgentName(), null);
		loadAgentLists();
	}
	
	public void onFolderDrop(DragDropEvent event) {
		if (selectedFolderNode.getFolder().getId() < 0) { return; } // Don't put agents in fake folder for root node in UI!
		Agent droppedAgent = (Agent)event.getData();
		agentService.setAgentFolder(droppedAgent.getAgentName(), selectedFolderNode.getFolder().getId());
		loadAgentLists();
	}
	
	public void createFolder() {
		newFolder.setIcon(newFolderIcon.getIcon());
		newFolder.setParent(selectedFolderNode.getFolder().getId() >= 0 ? selectedFolderNode.getFolder().getId() : null);
		agentService.createFolder(newFolder);
		
		AgentFolderTreeNode node = new AgentFolderTreeNode(newFolder);
		node.setParent(selectedFolderNode);
		selectedFolderNode.getChildren().add(node);
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Folder created: " + newFolder.getName(), null));
		
		newFolder = new AgentFolder();
		newFolderIcon = new FolderIconOption("folder.png", "folder.png");
	}
	
	public void renameSelectedFolder() {
		if (selectedFolderNode == null || selectedFolderNode.getFolder().getId() < 0) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "No folder or invalid folder selected for rename.", null));
			return;
		}
		
		String oldName = selectedFolderNode.getFolder().getName();
		
		agentService.renameFolder(selectedFolderNode.getFolder().getId(), renameFolderName);
		selectedFolderNode.getFolder().setName(renameFolderName);
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Folder " + oldName + " renamed to " + renameFolderName, null));
		
		renameFolderName = "";
	}
	
	public void deleteSelectedFolder() {
		if (selectedFolderNode == null || selectedFolderNode.getFolder().getId() < 0) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "No folder or invalid folder selected for delete.", null));
			return;
		}
		
		selectedFolderNode.getParent().getChildren().remove(selectedFolderNode);
		agentService.deleteFolder(selectedFolderNode.getFolder().getId());
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Folder deleted: " + selectedFolderNode.getFolder().getName(), null));
		
		selectedFolderNode = (AgentFolderTreeNode)agentFolderTree.getChildren().get(0);
		loadAgentLists();
	}
	
	public void addClassifier() {
		if (newClassifier == null || newClassifier.length() == 0) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to add empty classifier.", null));
			newClassifier = "";
			return;
		}
		
		if (selectedFolderNode == null || selectedFolderNode.getFolder().getId() < 0) {
			FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "No folder or invalid folder for adding classifier.", null));
			newClassifier = "";
			return;
		}
		
		ClassifierMappingFolder classifierMapping = new ClassifierMappingFolder();
		classifierMapping.setFolderId(selectedFolderNode.getFolder().getId());
		classifierMapping.setClassifier(newClassifier);
		
		agentService.addFolderClassifier(classifierMapping);
		selectedFolderClassifiers.add(classifierMapping);
		newClassifier = "";
		
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Classifier " + newClassifier + " added to folder " + selectedFolderNode.getFolder().getName(), null));
	}
	
	public void deleteClassifier() {
		if (deleteClassifierId == null) { return; }
		agentService.deleteFolderClassifier(deleteClassifierId);
		ClassifierMappingFolder removedClassifier = null;
		for (ClassifierMappingFolder cm : selectedFolderClassifiers) {
			if (cm.getId().equals(deleteClassifierId)) { removedClassifier = cm; }
		}
		
		if (removedClassifier != null) { selectedFolderClassifiers.remove(removedClassifier); }
	}
	
	protected void loadTree() {
		agentFolderTree = AgentFolderTreeNode.createTree(agentService.getAllFolders());
		selectedFolderNode = (AgentFolderTreeNode)agentFolderTree.getChildren().get(0);
		selectedFolderAgents = new ArrayList<>();
		rootFolderAgents = agentService.getAgentsInFolder(null);
	}
	
	protected void loadAgentLists() {
		selectedFolderAgents = agentService.getAgentsInFolder(selectedFolderNode.getFolder().getId());
		rootFolderAgents = agentService.getAgentsInFolder(null);
	}
}
