var rootFolders = [];
var classifierDeleteOn = false;

var classifiers = null;
var folders = null;
var currentFolder = null;

function initFolderUI() {
	// Buttonize buttons (JQuery UI)
	$('.btn').button();
	
	// Hide drop-down forms
	$('#createFolderSection').hide();
	
	initFolders();
	initClassifiers();
	initCreateFolderForm();
	initDeleteFolderAction();
	initDragAndDropAgents();
}

function initCreateFolderForm() {
	// Create folder form expose and hide
	$('#createFolderToolbarButton').click(function() {
		// Clear the form
		$('#newFolderName').val('');
		$('#newFolderIcon').val('folder.png');
		$('img.folderIconOption').removeClass('iconSelected');
		$('#folder\\.png').addClass('iconSelected');
		
		// Show the form
		$('#folderToolbar').hide();
		$('#createFolderSection').slideDown(400);
	});

	$('#createFolderCancel').click(function() {
		$('#createFolderSection').slideUp(200, function() {
			$('#folderToolbar').show();
		});
	});

	// Create folder form icon selection
	$('img.folderIconOption').click(function(event) {
		$('img.folderIconOption').removeClass('iconSelected');
		$(event.currentTarget).addClass('iconSelected');
		$('#newFolderIcon').val(event.currentTarget.id);
	});
}

function initDeleteFolderAction() {
	$('#deleteFolderToolbarButton').click(function(event) {
		$('#confirmDeleteDialog').dialog({
			width: 400,
			height: 250,
			resizable: false,
			modal: true,
			buttons: {
				'Confirm Delete': function() {
					folders.deleteItem(currentFolder);
				},
				'Cancel': function() {
					$(this).dialog('close');
				}
			},
		});
	});
}

function initDragAndDropAgents() {
	// Add drag/drop handling for agents
	handleAgentDrop = function(event, ui) {
		var agentName = ui.item.attr('data-agentName');
		var postData = { 'agentName' : agentName };
		// Let folderId be null if currentFolder is null by just not setting it
		if (event.currentTarget.id == 'folderAgents') { postData['folderId'] = currentFolder.id; }

		$.ajax({
			url: restAgentServicePath + "/setAgentFolder",
			type: 'POST',
			data: postData
		});
	};
	
	var folderAgents = $('#folderAgents');
	var rootAgents = $('#rootAgents');
	folderAgents.sortable();
	if (rootAgents) {
		rootAgents.sortable({
			connectWith: '#folderAgents'
		});
		folderAgents.sortable('option', 'connectWith', '#rootAgents');

		rootAgents.bind('sortreceive', handleAgentDrop);
		folderAgents.bind('sortreceive', handleAgentDrop);
	}
}

function initFolders() {
	folders = new RenderedCollection({
		container: $('#folderList'),
		loadURL: restAgentServicePath + '/getAllFolders',
		createURL: restAgentServicePath + '/createFolder',
		deleteURL: restAgentServicePath + '/deleteFolder',
		groupBy: 'parent',
		idProp: 'id',
		itemClass: 'folderItem',
		nullGroup: 'ROOT',
		postLoadDataSuccess: function() {
			selectFolder(-1);
		},
		renderItem: function(item) {
			var folderItem = $('<div></div>');
			var itemImg = $('<img class="itemIcon"/>');
			itemImg.attr('src', '/dsc/web/webres/img/foldericon/' + item.icon);
			folderItem.append(itemImg);
			var itemName = $('<span class="itemName"></span>');
			itemName.text(item.name);
			folderItem.append(itemName);
			
			folderItem.click(function(event) {
				selectFolder($(event.currentTarget).attr("data-id"));
			});
			
			return folderItem;
		},
		renderEmptyGroup: function(container) {
			container.text('This folder has no sub-folders.');
		},
		formBinding: {
			name: $('#newFolderName'),
			icon: $('#newFolderIcon'),
			parent: 'currentFolder == null ? null : currentFolder.id',
		},
		postAddItemSuccess: function(item) {
			$('#createFolderSection').slideUp(200, function() {
				$('#folderToolbar').show();
			});
		},
		postDeleteItemSuccess: function(item) {
			$('#confirmDeleteDialog').dialog('close');
			selectFolder(item.parent == null ? -1 : item.parent);
		}
	});
	
	$('#createFolderButton').click($.proxy(folders.addItemFromForm, folders));
	
	folders.loadData();
}

function initClassifiers() {
	// Deletion toggle
	$('#deleteClassifierCheckbox').change(function() {
		if ($('#deleteClassifierCheckbox').is(':checked') == true) {
			classifierDeleteOn = true;
			$('span.classifier').addClass('classifierDelete');
		} else {
			classifierDeleteOn = false;
			$('span.classifier').removeClass('classifierDelete');
		}
	});
	
	// Create rendered collection
	classifiers = new RenderedCollection({
		container: $('#currentClassifiers'),
		loadURL: restAgentServicePath + '/getAllFolderClassifiers',
		createURL: restAgentServicePath + '/addFolderClassifier',
		deleteURL: restAgentServicePath + '/deleteFolderClassifier',
		groupBy: 'folderId',
		idProp: 'id',
		itemClass: 'classifier',
		renderItem: function(item) {
			var elmt = $('<span></span>');
			elmt.text(item.classifier);
			elmt.click(function(event) {
				if (classifierDeleteOn) { classifiers.deleteItem($(this)); }
			});
			return elmt;
		},
		formBinding: {
			classifier: $('#newClassifierName'),
			folderId: 'currentFolder.id'
		},
		postAddItemSuccess: function(item) {
			$('#newClassifierName').val('');
		},
	});
	
	// Add classifier
	$('#addClassifierButton').click($.proxy(classifiers.addItemFromForm, classifiers));
	
	classifiers.loadData();
}

function createAgentItem(name) {
	var agentItem = $('<div class="agentItem"></div>');
	agentItem.attr('data-agentName', name);
	
	var agentIcon = $('<img class="itemIcon"/>');
	agentIcon.attr('src', '/dsc/web/webres/img/agents-32.png');
	agentItem.append(agentIcon);
	
	var agentName = $('<span class="itemName"></span>');
	agentName.text(name);
	agentItem.append(agentName);
	
	return agentItem;
}

function onFolderSelection() {
	if (currentFolder == null) { $('.rootSelectedHide').hide(); }
	else { $('.rootSelectedHide').show(); }
	
	folders.renderGroup(currentFolder == null ? 'ROOT' : currentFolder.id);
	
	// Create the folder chain (bread crumb)
	var spc = $('<span class="middleBlock">&#187;</span>');
	var folderChain = $('#folderChain');
	folderChain.empty();
	var chainItems = [];
	
	if (currentFolder != null) {
		chainItems.push(folders.renderAndTagItem(currentFolder));
		
		var parent = folders.getItemById(currentFolder.parent);
		while (parent != null) {
			chainItems.push(spc.clone());
			chainItems.push(folders.renderAndTagItem(parent));
			parent = folders.getItemById(parent.parent);
		}
		
		chainItems.push(spc);
	}
	
	// Push the folder chain items into the DOM
	for (var i = (chainItems.length - 1); i >= 0; i--) {
		folderChain.append(chainItems[i]);
	}
	
	// Populate the agent lists
	$.ajax({
		url: restAgentServicePath + '/getAgentsInFolder',
		type: 'GET',
		data: (currentFolder == null ? {} : { folderId: currentFolder.id }),
		success: function(data, textStatus, jqXHR) {
			var folderAgents = $('#folderAgents');
			folderAgents.empty();
			for (var i in data) {
				folderAgents.append(createAgentItem(data[i].agentName));
			}
		},
	});
	
	if (currentFolder != null) {
		// Load root folder agents
		$.ajax({
			url: restAgentServicePath + '/getAgentsInFolder',
			type: 'GET',
			data: {},
			success: function(data, textStatus, jqXHR) {
				var rootAgents = $('#rootAgents');
				rootAgents.empty();
				for (var i in data) {
					rootAgents.append(createAgentItem(data[i].agentName));
				}
			},
		});
		
		classifiers.renderGroup(currentFolder.id);
		
		// Reset classifier form
		$('#newClassifierName').val('');
		if (classifierDeleteOn == true) {
			var deleteClassifierCheckbox = $('#deleteClassifierCheckbox');
			deleteClassifierCheckbox.attr('checked', false);
			deleteClassifierCheckbox.button('refresh');
			classifierDeleteOn = false;
		}
	}
}

function selectFolder(id) {
	currentFolder = id == -1 ? null : folders.getItemById(id);
	onFolderSelection();
}

$(document).ready(function() {
	initFolderUI();
});