var VALUE_TYPE_STRING = 1;
var VALUE_TYPE_JSON = 2;

var VALUE_TYPE = {
	1: 'String',
	2: 'JSON'
};

var EDIT_STATE_NO_SELECTION = 'NO_SELECTION';
var EDIT_STATE_ITEM_SELECTED_STRING = 'ITEM_SELECTED_STRING';
var EDIT_STATE_ITEM_SELECTED_JSON = 'ITEM_SELECTED_JSON';
var EDIT_STATE_NEW_ITEM = 'NEW_ITEM';
var EDIT_STATE_MULTI_SELECT = 'MULTI_SELECT';

var lastSearch = null;

var dataModel = null;
var dataGrid = null;
var dataGridDiv = null;
var tabSearch = null;
var tabEdit = null;

var editor = null;
var editedItem = null;
var editDiv = null;
var editorShown = false;

var editForm = null;
var editFormDiv = null;
var valueEditDialog = null;
var editButtons = {};
var editingNew = false;

var newItem = null;
var addingNewValue = false;

// Used by slickgrid to format the Value Type column
var ValueTypeFormatter = function(row, cell, value, columnDef, dataContext) {
	return VALUE_TYPE[value];
};

// Data model for slick grid
var DataModel = function(data) {
	this.$data = data;
	
	this.getItem = function(index) {
		return this.$data[index];
	};
	
	this.getLength = function() {
		return this.$data.length;
	};
	
	this.setItem = function(index, item) {
		this.$data[index] = item;
	};
	
	this.addItem = function(item) {
		var idx = this.$data.length;
		this.$data.push(item);
		return idx;
	};
	
	this.deleteItem = function(index) {
		this.$data.splice(index, 1);
	};
};

// Show All button clicked
var showAllButtonClick = function(event) {
	$('#searchFilter').val('');
	doSearch(null);
};

// Search button clicked
var searchButtonClick = function(event) {
	var searchFilter = $('#searchFilter');
	var keyPattern = searchFilter.val();
	
	if (keyPattern.length > 0) {
		doSearch(keyPattern);
	} else {
		searchFilter.effect('highlight', { color: '#FFDDDD' }, 300);
	}
};

// Class backend for key search
function doSearch(keyPattern) {
	lastSearch = keyPattern;
	
	DataService.getDataByKeyPattern({
		keyPattern: keyPattern,
		$callback: function(httpCode, xhr, value) {
			dataModel = new DataModel(value);
			dataGrid.setData(dataModel);
			dataGrid.updateRowCount();
			dataGrid.render();
			
			dataGrid.setSelectedRows([]);
			editingNew = false;
			setEditButtonState(EDIT_STATE_NO_SELECTION);
		}
	});
}

// Toolbar - New clicked
var tbNewClicked = function(event) {
	editForm.clear();
	editingNew = true;
	setEditButtonState(EDIT_STATE_NEW_ITEM);
};

// Toolbar - Edit Value clicked
var tbEditClicked = function(event) {
	if (editedItem != null) {
		if (editedItem.valueTypeInt == VALUE_TYPE_STRING) {
			valueEditDialog.dialog('option', 'title', "Edit Value");
			$('#valueEditKey').text(editedItem.key);
			$('#newValue').val(editedItem.displayValue);
			valueEditDialog.dialog('open');
		} else if (editedItem.valueTypeInt == VALUE_TYPE_JSON) {
			editorLoadValue(editedItem);
		}
	}
};

// Toolbar - Save clicked
var tbSaveClicked = function(event) {
	if (editingNew == true) {
		// Save new data item
		editedItem = editForm.getObj();
		DataService.createData({
			$entity: editedItem,
			$callback: function(httpCode, xhr, value) {
				editedItem = value;
				var idx = dataModel.addItem(editedItem);
				
				dataGrid.updateRowCount();
				dataGrid.render();
				
				dataGrid.setSelectedRows([idx]);
			},
		});
	} else {
		// Update existing item
		var editedRow = dataGrid.getSelectedRows()[0];
		var updateItem = editForm.getObj(); // Get fresh object from form to avoid sending displayValue back
		editForm.mergeObj(editedItem);
		updateItem.id = editedItem.id;
		
		DataService.updateData({
			$entity: updateItem,
			$callback: function(httpCode, xhr, value) {
				dataGrid.invalidateRow(editedRow);
				dataGrid.render();
			},
		});
	}
};

// Toolbar - Add clicked - prompts for value and saves as a new record
var tbAddClicked = function(event) {
	if (editedItem != null) {
		// Create new data item based off the currently edited item
		newItem = editForm.getObj();
		addingNewValue = true;
		
		valueEditDialog.dialog('option', 'title', "Add Value");
		$('#valueEditKey').text(editedItem.key);
		$('#newValue').val('');
		valueEditDialog.dialog('open');
	}
};

var tbDeleteClicked = function(event) {
	// FIXME - Add confirm dialog!
	var deleteIds = [];
	var deleteIndexes = [];
	var rows = dataGrid.getSelectedRows();
	
	for (var idx in rows) {
		deleteIndexes.push(rows[idx]);
		deleteIds.push(dataModel.getItem(rows[idx]).id);
	}
	
	DataService.deleteDataByIds({
		$entity: deleteIds,
		$callback: function(httpCode, xhr, value) {
			if (deleteIndexes.length == 1) {
				dataModel.deleteItem(deleteIndexes[0]);
				dataGrid.invalidate();
				dataGrid.updateRowCount();
				dataGrid.render();
			} else {
				doSearch(lastSearch);
			}
		},
	});
};

// Value Edit Dialog - Save Clicked
var valueEditSaveButtonClick = function(event) {
	var newValue = $('#newValue').val();
	
	if (addingNewValue) {
		// Added value to existing entry

		// Create data item
		DataService.createData({
			$entity: newItem,
			$callback: function(httpCode, xhr, value) {
				editedItem = value;
				editedItem.displayValue = newValue;
				addingNewValue = false;
				
				var idx = dataModel.addItem(editedItem);
				
				// After item created, write its value
				DataService.saveValue({
					id: editedItem.id.toString(),
					value: newValue,
					$callback: function(httpCode, xhr, value) {
						valueEditDialog.dialog('close');
						dataGrid.updateRowCount();
						dataGrid.render();
						
						dataGrid.setSelectedRows([idx]);
					},
				});
			},
		});
	} else {
		// Edited value of existing entry
		editedItem.displayValue = newValue;
		var editedRow = dataGrid.getSelectedRows()[0];
		
		DataService.saveValue({
			id: editedItem.id.toString(),
			value: newValue,
			$callback: function(httpCode, xhr, value) {
				valueEditDialog.dialog('close');
				dataGrid.invalidateRow(editedRow);
				dataGrid.render();
			},
		});
	}
};

// Value Edit Dialog - Cancel Clicked
var valueEditCancelButtonClick = function(event) {
	addingNewValue = false;
	valueEditDialog.dialog('close');
};

// Handler for grid selection change event
var gridEventSelectionChanged = function(event, args) {
	if (args.rows.length == 1) {
		editedItem = dataModel.getItem(args.rows[0]);
		editForm.loadObj(editedItem);
		if (editedItem.valueTypeInt == VALUE_TYPE_JSON) { setEditButtonState(EDIT_STATE_ITEM_SELECTED_JSON); }
		else if (editedItem.valueTypeInt == VALUE_TYPE_STRING) { setEditButtonState(EDIT_STATE_ITEM_SELECTED_STRING); }
	} else if (args.rows.length > 1) {
		editForm.clear();
		setEditButtonState(EDIT_STATE_MULTI_SELECT);
	} else if (args.rows.length == 0) {
		setEditButtonState(EDIT_STATE_NO_SELECTION);
	}
	
	editingNew = false;
};

// Save Changes button cliecked in editor
var editorSaveButtonClick = function(event) {
	var newValue = editor.getSession().getValue();
	DataService.saveValue({
		id: editedItem.id.toString(),
		value: newValue,
		$callback: function(httpCode, xhr, value) {
			editor.getSession().setValue("");
			showTabSearch();
		},
	});
};

// Discard clicked in editor tab
var editorDiscardButtonClick = function(event) {
	showTabSearch();
};

// Load a data item's value into the ACE editor
function editorLoadValue(item) {
	DataService.getValueById({
		id: item.id.toString(),
		$callback: function(httpCode, xmlHttpRequest, value) {
			editor.getSession().setMode("ace/mode/json");
			editor.getSession().setValue(value);
			showTabEditor();
		},
	});
}

// Show the editor tab
function showTabEditor() {
	tabSearch.hide();
	tabEdit.show();
	computeResize();
	editorShown = true;
}

//Show the search tab
function showTabSearch() {
	tabEdit.hide();
	tabSearch.show();
	computeResize();
	editorShown = false;
}

// Handle a browser resize and recompute element sizes
var computeResize = function() {
	setTimeout(function() {
		if (editorShown) {
			// Resize editor
			editDiv.height(window.innerHeight - editDiv.position().top);
			editDiv.width(window.innerWidth - editDiv.position().left);
			editor.resize();
		} else {
			// Resize search result grid
			dataGridDiv.height(window.innerHeight - dataGridDiv.position().top - editFormDiv.outerHeight() - 1);
			dataGridDiv.width(window.innerWidth - dataGridDiv.position().left);
			dataGrid.resizeCanvas();
		}
	}, 0);
};

function initUI() {
	// Global DOM elements
	editDiv = $('#editor');
	tabSearch = $('#tabSearch');
	tabEdit = $('#tabEdit');
	dataGridDiv = $('#dataGrid');
	editFormDiv = $('#editForm');
	valueEditDialog = $('#valueEditDialog');
	
	// Buttonize
	$('.btn').button();
	
	// Value edit dialog
	valueEditDialog.dialog({
		autoOpen: false,
		modal: true,
		width: 400,
	});
	$('#valueEditSaveButton').click(valueEditSaveButtonClick);
	$('#valueEditCancelButton').click(valueEditCancelButtonClick);
	
	// Hide things
	tabEdit.hide();
	valueEditDialog.hide();
	
	// Event Bindings - Search
	$('#showAllButton').click(showAllButtonClick);
	$('#searchButton').click(searchButtonClick);
	$('#searchFilter').keyup(function(event) {
		if (event.keyCode == 13) { searchButtonClick(); }
		else if (event.keyCode == 27) { $(this).val(''); }
	});
	
	// Event Bindings - Data Editor
	$('#editorSaveButton').click(editorSaveButtonClick);
	$('#editorDiscardButton').click(editorDiscardButtonClick);
	
	// Window resize events
	$(window).resize(function() { computeResize(); });
	
	// Edit Form Binding
	editForm = new Form(editFormDiv);
}

function initGrid() {
	var columns = [
		{ id: 'key', name: 'Key', field: 'key', width: 180 },
		{ id: 'classifier', name: 'Classifier', field: 'classifier', width: 120 },
		{ id: 'valueType', name: 'Value Type', field: 'valueTypeInt', width: 80, formatter: ValueTypeFormatter },
		{ id: 'displayValue', name: 'Value', field: 'displayValue', width: 350 },
		{ id: 'jsonDefaultsKey', name: "JSON Defaults Key", field: 'jsonDefaultsKey', width: 180 }
	];
	
	var options = {
		enableCellNavigation: true,
		enableColumnReorder: false,
		autoHeight: false,
		enableTextSelectionOnCells: true,
		editable: false,
		autoEdit: false,
		forceFitColumns: true,
	};
	
	dataGrid = new Slick.Grid('#dataGrid', [], columns, options);
	dataGrid.setSelectionModel(new Slick.RowSelectionModel());
	
	dataGrid.onSelectedRowsChanged.subscribe(gridEventSelectionChanged);
}

function initEditButtons() {
	editButtons = {
		'new': $('#tbNewButton'),
		'save': $('#tbSaveButton'),
		'add': $('#tbAddButton'),
		'copy': $('#tbCopyButton'),
		'delete': $('#tbDeleteButton'),
		'edit': $('#tbEditButton'),
	};
	
	setEditButtonState(EDIT_STATE_NO_SELECTION);
	
	// Events
	editButtons['new'].click(tbNewClicked);
	editButtons['save'].click(tbSaveClicked);
	editButtons['add'].click(tbAddClicked);
	editButtons['delete'].click(tbDeleteClicked);
	editButtons['edit'].click(tbEditClicked);
}

function setEditButtonState(state) {
	if (state == EDIT_STATE_NO_SELECTION) {
		editButtons['new'].button('enable');
		editButtons['save'].button('disable');
		editButtons['add'].button('disable');
		editButtons['copy'].button('disable');
		editButtons['delete'].button('disable');
		editButtons['edit'].button('disable');
	} else if (state == EDIT_STATE_ITEM_SELECTED_STRING) {
		editButtons['new'].button('enable');
		editButtons['save'].button('enable');
		editButtons['add'].button('enable');
		editButtons['copy'].button('enable');
		editButtons['delete'].button('enable');
		editButtons['edit'].button('enable');
	} else if (state == EDIT_STATE_ITEM_SELECTED_JSON) {
		editButtons['new'].button('enable');
		editButtons['save'].button('enable');
		editButtons['add'].button('disable');
		editButtons['copy'].button('enable');
		editButtons['delete'].button('enable');
		editButtons['edit'].button('enable');
	} else if (state == EDIT_STATE_NEW_ITEM) {
		editButtons['new'].button('disable');
		editButtons['save'].button('enable');
		editButtons['add'].button('disable');
		editButtons['copy'].button('disable');
		editButtons['delete'].button('disable');
		editButtons['edit'].button('disable');
	} else if (state == EDIT_STATE_MULTI_SELECT) {
		editButtons['new'].button('enable');
		editButtons['save'].button('disable');
		editButtons['add'].button('disable');
		editButtons['copy'].button('disable');
		editButtons['delete'].button('enable');
		editButtons['edit'].button('disable');
	}
}

function initEditor() {
	editor = ace.edit('editor');
	editor.setTheme('ace/theme/eclipse');
	editor.setShowPrintMargin(false);
	editor.getSession().setNewLineMode('unix');
}

$(document).ready(function() {
	initUI();
	initGrid();
	initEditor();
	initEditButtons();
});

$(window).load(function() {
	computeResize();
});