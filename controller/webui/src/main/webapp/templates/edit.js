
var TAB_SEARCH = 1;
var TAB_EDITOR = 2;

var lastSearch = null;
var templateListDiv = null;
var templateListData = [];

var searchTab = null;
var editTab = null;

var newTemplateDialog = null;

var editDiv = null;
var editor = null;
var editedTemplate = null;

var activeTab = TAB_SEARCH;

var initUI = function() {
	// Look up DOM objects
	templateListDiv = $('#templateListDiv');
	newTemplateDialog = $('#newTemplateDialog');
	searchTab = $('#tabSearch');
	editTab = $('#tabEdit');
	editDiv = $('#editor');
	
	// Buttonize
	$('.btn').button();
	
	// Hide editor
	editTab.hide();
	
	// Window resize events
	$(window).resize(function() { computeResize(); });
};

// Setup UI controls for search tab
var initSearch = function() {
	$('#createButton').click(function() {
		$('#newTemplateName').val('');
		newTemplateDialog.dialog('open');
	});
	
	newTemplateDialog.dialog({
		autoOpen: false,
		modal: true,
		width: 400,
		title: "New Template",
	});
	
	$('#newTemplateSaveButton').click(newTemplateSaveButtonClick);
	$('#newTemplateCancelButton').click(newTemplateCancelButtonClick);
	
	$('#showAllButton').click(function(event) {
		doSearch(null);
	});
};

var initEditor = function() {
	editor = ace.edit('editor');
	editor.setShowPrintMargin(false);
	editor.setTheme('ace/theme/eclipse');
	editor.getSession().setNewLineMode('unix');
	
	$('#editorSaveButton').click(editorSaveButtonClick);
	$('#editorSaveCloseButton').click(editorSaveCloseButtonClick);
	$('#editorDiscardButton').click(editorDiscardButtonClick);
};

// Handle a browser resize and recompute element sizes
var computeResize = function() {
	setTimeout(function() {
		if (activeTab == TAB_EDITOR) {
			// Resize editor
			editDiv.height(window.innerHeight - editDiv.position().top);
			editDiv.width(window.innerWidth - editDiv.position().left);
			editor.resize();
		}
	}, 0);
};

var renderTemplateList = function() {
	templateListDiv.empty();
	
	var table = $('<table id="templateListTable"></table>');
	table.attr('class', 'listTable');
	table.css('width', '100%');
	
	for (var i in templateListData) {
		var templateRow = $('<tr></tr>');
		
		var templateColName = $('<td></td>');
		templateColName.text(templateListData[i].templateName);
		templateColName.css('width', '100%');
		
		var templateColEdit = $('<td></td>');
		
		var templateColEditButton = $('<span id="edit-' + templateListData[i].id + '"></span>');
		templateColEditButton.text('Edit');
		templateColEditButton.addClass('btn');
		templateColEditButton.attr('data-id', templateListData[i].id);
		templateColEditButton.button();
		templateColEditButton.click(editTemplateButtonClick);
		
		templateColEdit.append(templateColEditButton);
		
		var templateColDelete = $('<td></td>');
		var templateColDeleteButton = $('<span id="delete-' + templateListData[i].id + '"></span>');
		templateColDeleteButton.text('Delete');
		templateColDeleteButton.addClass('btn');
		templateColDeleteButton.attr('data-id', templateListData[i].id);
		templateColDeleteButton.button();
		templateColDeleteButton.click(deleteTemplateButtonClick);
		
		templateColDelete.append(templateColDeleteButton);
		
		var templateColTest = $('<td></td>');
		var templateColTestButton = $('<span id="test-' + templateListData[i].id + '"></span>');
		templateColTestButton.text("Test");
		templateColTestButton.addClass('btn');
		templateColTestButton.button();
		templateColTestButton.attr('data-id', templateListData[i].id);
		templateColTestButton.click(testTemplateButtonClick);
		
		templateColTest.append(templateColTestButton);
		
		templateRow.append(templateColName);
		templateRow.append(templateColEdit);
		templateRow.append(templateColDelete);
		templateRow.append(templateColTest);
		
		table.append(templateRow);
	}
	
	templateListDiv.append(table);
};

//Search for templates
var doSearch = function(namePattern) {
	lastSearch = namePattern;
	
	TemplateService.getTemplatesByNamePattern({
		namePattern: namePattern,
		$callback: function(httpCode, xhr, value) {
			templateListData = value;
			renderTemplateList();
		}
	});
};

var newTemplateSaveButtonClick = function(event) {
	var newTemplateName = $('#newTemplateName').val();
	if (newTemplateName.length > 0) {
		var newTemplate = { templateName: newTemplateName };
		TemplateService.createTemplate({
			$entity: newTemplate,
			$callback: function(httpCode, xhr, value) {
				newTemplateDialog.dialog('close');
			},
		});
	}
};

var newTemplateCancelButtonClick = function(event) {
	newTemplateDialog.dialog('close');
};

// Edit button clicked on some row in the template list
var editTemplateButtonClick = function(event) {
	// Figure out what template ID was selected from the DOM element
	var id = event.target.parentElement.dataset.id;
	
	for (var i in templateListData) {
		if (templateListData[i].id == id) { editedTemplate = templateListData[i]; }
	}
	
	editor.getSession().setMode('ace/mode/text');
	editor.getSession().setValue("");
	
	TemplateService.loadTemplateData({
		id: id,
		$callback: function(httpCode, xhr, value) {
			$('#templateName').val(editedTemplate.templateName);
			editor.getSession().setValue(value);
			showTab(TAB_EDITOR);
		},
	});
};

var deleteTemplateButtonClick = function(event) {
	// FIXME - Add confirm dialog!
	var id = event.target.parentElement.dataset.id;
	
	TemplateService.deleteTemplate({
		id: id,
		$callback: function(httpCode, xhr, value) {
			doSearch(lastSearch);
		},
	});
};

var testTemplateButtonClick = function(event) {
	var id = event.target.parentElement.dataset.id;
};

// Saves the template, updating the name if it has changed
var saveTemplate = function(closeEditor) {
	var newName = $('#templateName').val();
	
	if (newName != editedTemplate.templateName) {
		if (newName.length > 0) {
			editedTemplate.templateName = newName;
			
			TemplateService.updateTemplate({
				$entity: editedTemplate,
				$callback: function(httpCode, xhr, value) {
					saveEditorData(closeEditor);
				},
			});
		}
	} else {
		saveEditorData(closeEditor);
	}
};

// Saves the editor data for the currently edited template
var saveEditorData = function(closeEditor) {
	var templateData = editor.getSession().getValue();
	TemplateService.saveTemplateData({
		id: editedTemplate.id.toString(),
		data: templateData,
		$callback: function(httpCode, xhr, value) {
			if (closeEditor) {
				showTab(TAB_SEARCH);
			} else {
				alert("Template Saved: " + editedTemplate.templateName);
			}
		},
	});
};

var editorSaveButtonClick = function(event) {
	saveTemplate(false);
};

var editorSaveCloseButtonClick = function(event) {
	saveTemplate(true);
};

var editorDiscardButtonClick = function(event) {
	showTab(TAB_SEARCH);
};

var showTab = function(tab) {
	if (tab == TAB_SEARCH) {
		editTab.hide();
		searchTab.show();
		activeTab = TAB_SEARCH;
		doSearch(lastSearch);
	} else if (tab == TAB_EDITOR) {
		searchTab.hide();
		editTab.show();
		activeTab = TAB_EDITOR;
	}
	
	computeResize();
};

$(document).ready(function() {
	initUI();
	initSearch();
	initEditor();
});