function Form(formElement) {
	this.$formElement = formElement;
	
	this.bindings = {};
	// Examine form for bindings
	var elements = this.$formElement.find(':input').toArray();
	for (var i in elements) {
		var elmt = $(elements[i]);
		if (elmt.attr('data-prop') != undefined) {
			this.bindings[elmt.attr('data-prop')] = elmt;
		}
	}
	
	// Call to load an object into the form, this sets all bound form field
	// values to those contained in the object
	this.loadObj = function(obj) {
		for (var prop in this.bindings) {
			this.bindings[prop].val(obj[prop]);
		}
		
		this.postLoad(obj);
	};
	
	// Produce an object with its state set by the bound form fields
	this.getObj = function() {
		var obj = {};
		for (var prop in this.bindings) {
			obj[prop] = this.bindings[prop].val();
		}
		return obj;
	};
	
	// Merge the form values into an existing object, without modifying properties that do not exist in the form
	// Useful for when an object contains a surrogate key and the form does not
	this.mergeObj = function(obj) {
		for (var prop in this.bindings) {
			obj[prop] = this.bindings[prop].val();
		}
	};
	
	// Override to add behavior that should execute after an object is loaded into the form
	this.postLoad = function(obj) { };
	
	// Set bound form fields to their default values
	this.clear = function() {
		for (var prop in this.bindings) {
			var def = this.bindings[prop].attr('data-default');
			if (def == undefined) { def = ''; }
			this.bindings[prop].val(def);
		}
	};
}