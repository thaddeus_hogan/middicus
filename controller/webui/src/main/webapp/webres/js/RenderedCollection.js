/*
 * RenderedCollection - Provides a means of storing a collation of objects
 * which will be rendered to the DOM. Supports common tasks of grouping, addition,
 * removal, and retrieval from a REST endpoint.
 */
function RenderedCollection(config) {
	this.container = config.container; // Must be a JQuery object
	if ((config.container instanceof jQuery) == false) {
		console.log("RenderedCollection was passed a container object that was not a jQuery object");
	}
	
	if (config.hasOwnProperty('groupBy') == true) {
		this.groupBy = config.groupBy;
		if (config.hasOwnProperty('nullGroup') == true) {
			this.nullGroup = config.nullGroup;
		} else {
			this.nullGroup = 'NULL';
		}
		this.store = {};
	} else {
		this.store = [];
		this.groupBy = null;
	}
	
	// The property of the items that represents its identity
	this.idProp = config.hasOwnProperty('idProp') ? config.idProp : null;
	if (this.idProp == null) {
		console.log("Automatic handling of REST create and delete will not work unless idProp is specified");
	}
	
	// itemClass - The class that will be added to any item rendered by this RenderedCollection
	this.itemClass = config.itemClass;
	if (config.hasOwnProperty('itemClass') == false) {
		console.log("RenderedCollection must have itemClass specified.");
	}
	
	// formBinding - A map that can contain jQuery objects or Strings, keyed by entity properties.
	// Tells this RenderedCollection how to create an item from a form
	// If the binding value is a jQuery object, the result of .val() on the object is used for the item
	// If the binding value is a String, the string is run through eval() and the result is used as the value for the item
	this.formBinding = config.hasOwnProperty('formBinding') ? config.formBinding : null;
	
	// Override to provide a function that can render the contents of an empty group when renderGroup() is called
	this.renderEmptyGroup = config.hasOwnProperty('renderEmptyGroup') ? config.renderEmptyGroup : function(container) {};
	
	// =================================================================================
	// ===== DATA LOAD AND EDIT
	// =================================================================================
	
	// URLs for data access and modification
	this.loadURL = config.loadURL;
	this.createURL = config.createURL;
	this.deleteURL = config.deleteURL;
	
	// ===== DATA LOAD
	
	// Data load behavior, override as neccessary. Avoid overriding loadDataSuccess to avoid re-inventing the wheel
	// Use preLoadData and postLoadDataSuccess for display updates
	this.loadDataSuccess = function(data, textStatus, jqXHR) {
		if (this.groupBy != null) {
			this.store = {};
			
			for (var i in data) {
				var group = data[i][this.groupBy];
				if (group == null) { group = this.nullGroup; }
				if (this.store.hasOwnProperty(group) == false) {
					this.store[group] = [];
				}
				this.store[group].push(data[i]);
			}
		} else {
			this.store = data;
		}
		
		this.postLoadDataSuccess();
	};
	
	// Stub methods for controlling behavior around loadData()
	this.preLoadData = config.hasOwnProperty('preLoadData') ? config.preLoadData : function() {};
	this.postLoadDataSuccess = config.hasOwnProperty('postLoadDataSuccess') ? config.postLoadDataSuccess : function() {};
	this.loadDataFailure = config.hasOwnProperty('loadDataFailure') ? config.loadDataFailure : function(jqXHR, textStatus, errorThrown) {};
	
	// Loads the collection from a REST endpoint
	this.loadData = function() {
		this.preLoadData();
		$.ajax({
			url: this.loadURL,
			type: 'GET',
			success: $.proxy(this.loadDataSuccess, this),
			error: $.proxy(this.loadDataFailure, this)
		});
	};
	
	// ===== DATA CREATE
	this.addItem = function(item) {
		this.preAddItem(item);
		var itemJson = JSON.stringify(item);
		$.ajax({
			url: this.createURL,
			type: 'POST',
			contentType: 'application/json',
			data: itemJson,
			success: $.proxy(this.addItemSuccess, this),
			error: $.proxy(this.addItemFailure, this)
		});
	};
	
	this.addItemSuccess = function(data, textStatus, jqXHR) {
		var firstItem = false;
		
		if (this.groupBy != null) {
			var group = data[this.groupBy] == null ? this.nullGroup : data[this.groupBy];
			if (this.store.hasOwnProperty(group) == false) {
				this.store[group] = [];
			}
			this.store[group].push(data);
			if (this.store[group].length == 1) { firstItem = true; }
		} else {
			this.store.push(data);
			if (this.store.length == 1) { firstItem = true; }
		}
		
		this.postAddItemSuccess(data);
		
		if (firstItem) { this.container.empty(); }
		this.container.append(this.renderAndTagItem(data));
	};
	
	this.preAddItem = config.hasOwnProperty('preAddItem') ? config.preAddItem : function(item) {};
	this.postAddItemSuccess = config.hasOwnProperty('postAddItemSuccess') ? config.postAddItemSuccess : function(item) {};
	this.addItemFailure = config.hasOwnProperty('addItemFailure') ? config.addItemFailure : function(jqXHR, textStatus, errorThrown) {};
	
	// Add an item using the information in a bound form
	this.addItemFromForm = function() {
		if (this.formBinding == null) {
			console.log("addItemFromForm called but formBinding is null");
			return;
		}
		
		var item = {};
		for (var key in this.formBinding) {
			// If bindings are jQuery objects, execute val() on them
			// Otherwise run the binding string through eval()
			if (this.formBinding[key] instanceof jQuery) {
				item[key] = this.formBinding[key].val();
			} else {
				item[key] = eval(this.formBinding[key]);
			}
		}
		
		this.addItem(item);
	};
	
	// ===== DATA DELETE
	this.deleteItem = function(item) {
		// Support accepting a jQuery object if idProp is set
		if (this.idProp != null && item instanceof jQuery) {
			item = this.getItemById(item.attr('data-id'));
		}
		
		this.preDeleteItem(item);
		
		// Find page element
		var elmt = $('.' + this.itemClass + '[data-id="' + item[this.idProp] + '"]');
		if ((elmt instanceof jQuery) == false) {
			alert("Cannot delete item, failed to find page element.");
			return;
		}
		
		// Submit the delete REST request
		var postData = {};
		postData[this.idProp] = item[this.idProp];
		
		$.ajax({
			url: this.deleteURL,
			type: 'POST',
			data: postData,
			success: $.proxy(function(data, textStatus, jqXHR) {
				this.deleteItemSuccess(data, textStatus, jqXHR, elmt);
				this.removeItemById(item[this.idProp]);
				this.postDeleteItemSuccess(item);
			}, this),
			error: $.proxy(this.deleteItemFailure, this),
		});
	};
	
	this.deleteItemSuccess = function(data, textStatus, jqXHR, elmt) {
		elmt.remove();
	};
	
	this.preDeleteItem = config.hasOwnProperty('preDeleteItem') ? config.preDeleteItem : function(item) {};
	this.postDeleteItemSuccess = config.hasOwnProperty('postDeleteItemSuccess') ? config.postDeleteItemSuccess : function(item) {};
	this.deleteItemFailure = config.hasOwnProperty('deleteItemFailure') ? config.deleteItemFailure : function(jqXHR, textStatus, errorThrown) {};
	
	// =================================================================================
	// ===== RENDERING
	// =================================================================================
	
	// Override to do something before rendering begins
	this.preRender = config.hasOwnProperty('preRender') ? config.preRender : function() {};
	// Override to do something after rendering is complete
	this.postRender = config.hasOwnProperty('postRender') ? config.postRender : function() {};
	
	// Override to return a jQuery object representing an element to be added to the DOM
	// which can represent an object in the store
	this.renderItem = config.hasOwnProperty('renderItem') ? config.renderItem : function(item) { console.log("RenderedCollection missing a renderItem() method"); };
	
	this.renderAndTagItem = function(item) {
		var elmt = this.renderItem(item);
		elmt.addClass(this.itemClass);
		if (this.idProp != null) {
			elmt.attr('data-id', item[this.idProp]);
		}
		
		return elmt;
	};
	
	// Clear the container and render a single item from the store array
	this.renderSingleItem = function(index) {
		this.preRender();
		
		this.container.empty();
		var item = this.store[index];
		if (item != null) {
			this.container.append(this.renderAndTagItem(item));
		}
		
		this.postRender();
	};
	
	// Clear the container and render a group of items from the store array
	this.renderGroup = function(groupKey) {
		this.preRender();
		
		this.container.empty();
		var group = this.store[groupKey];
		if (group == null || group.length == 0) {
			this.renderEmptyGroup(this.container);
		} else {
			for (var i in group) {
				this.container.append(this.renderAndTagItem(group[i]));
			}
		}
		
		this.postRender();
	};
	
	// =================================================================================
	// ===== DATA FUNCTIONS
	// =================================================================================
	
	// Searches the store and retrieves the item with the specified ID
	this.getItemById = function(id) {
		if (id == null) { return null; }
		var item = null;
		
		if (this.groupBy != null) {
			grploop: for (var group in this.store) {
				for (var i in this.store[group]) {
					if (this.store[group][i][this.idProp].toString() == id.toString()) {
						item = this.store[group][i];
						break grploop;
					}
				}
			}
		} else {
			for (var i in this.store) {
				if (this.store[i][this.idProp].toString() == id.toString()) {
					item = this.store[i];
					break;
				}
			}
		}
		
		return item;
	};
	
	this.removeItemById = function(id) {
		if (this.groupBy != null) {
			grploop: for (var group in this.store) {
				for (var i in this.store[group]) {
					if (this.store[group][i][this.idProp].toString() == id.toString()) {
						this.store[group].splice(i, 1);
						break grploop;
					}
				}
			}
		} else {
			for (var i in this.store) {
				if (this.store[i][this.idProp].toString() == id.toString()) {
					this.store.splice(i, 1);
					break;
				}
			}
		}
	};
}